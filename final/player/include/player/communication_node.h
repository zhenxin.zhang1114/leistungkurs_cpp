#ifndef COMMUNICATION_NODE_H
#define COMMUNICATION_NODE_H

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#endif
#include <QTimer>
#include <QWidget>
#include <QThread>
#include <QApplication>
#include <QMap>
#include <string>

#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/Point.h>

#include "player/command.h"
#include "player/a_and_b.h"
#include "player/start.h"
#include "../include/player/main_window.h"
#include "referee.h"


class CommunicationNode
{
public:
    CommunicationNode(int argc, char** argv, ros::NodeHandle nh);
    virtual ~CommunicationNode();
    MainWindow *mainwindow;
    // Public function of Publisher
    bool PublishStartStopCommand(bool flag_start_stop);
    // bool PublishStartStopCommand(const std_msgs::BoolConstPtr &flag_start_stop);
    // callbackfunctions for Subscribers
    void ReportCurrentPositionCallback(const geometry_msgs::PointConstPtr &current_position);
    void ReportAliveStatusCallback(const std_msgs::BoolConstPtr &isalived);
    // callback functions for services
    bool DetermineCommands(player::command::Request &req, player::command::Response &res);
    bool DetermineFieldSize(player::a_and_b::Request &req, player::a_and_b::Response &res);
    // callback functions for Timer
    void RunPubTimerCallback(const ros::TimerEvent &event);

    int init_argc;
    char** init_argv;

    // bool flag_start_stop_;
    bool flag_ready_reported_;
    bool flag_color_reported_;
    bool flag_abratio_reported_;
    std_msgs::Bool start_stop_;
    QString angelina_uri;
    double init_ratio;
    double received_ratio;
    // Publisher
    ros::Publisher start_stop_publisher_;
    // Timer for upper publisher;
    ros::Timer publisher_timer_;
    // The following 2 Services are added after meeting with collegues
    ros::ServiceServer detection_color_server_;
    ros::ServiceServer field_size_server_;
    // Subscriber
    ros::Subscriber current_position_subscriber_;
    // ros::Subscriber alive_status_subscriber_;
    // Client for feedback a start signal to Turtlebot
    ros::ServiceClient feedback_start_client_;
};
#endif /* COMMUNICATION_NODE_H */
