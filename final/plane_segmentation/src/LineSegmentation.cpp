#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Char.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


// PCL
#include <pcl_ros/point_cloud.h> //ROS_publish
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/common_headers.h>


#include <image_geometry/pinhole_camera_model.h>
#include <geometry_msgs/PoseStamped.h>
#include <vector>

#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <std_msgs/Float64MultiArray.h>

//service
#include "plane_segmentation/initial_Loc.h"
#include "plane_segmentation/next_Loc.h"

using namespace std;
using namespace cv;

//Konstante Variable einstellen
const float EPSILON=0.05;
//const double bias=0.03;
const double bias = 0.06;
const float disconst=2.5;
//const float disconst=3.5;
const float biascoor=0.2;
const float bias13 = 0.07;
const float bias12 = 0.05;
const float thetabias = 0.1;
const float anglebias = 0.2;


bool firsttime = true;
int times_twoline = 0;
int indextheta = 0;
vector<float> thetavector = {0,0};

class LineSegmentation
{

private:
    //! The node handle
    ros::NodeHandle nh_;
    //! Node handle in the private namespace
    ros::NodeHandle priv_nh_;

    //Lidar Signale empfangen
    ros::Subscriber sub_pc;

    ros::ServiceServer next_Loc;
    ros::ServiceServer initial_Loc;

    std_msgs::Float64 angle_ros;
    std_msgs::Float64 x_ros;
    std_msgs::Bool check_s;
    ros::Publisher pub_line1;
    ros::Publisher pub_line2;
    ros::Publisher pub_pointmean;

    bool initial_loccheck = false;
    //bool initial_loccheck = true;

    //Grenzlinie 1 and 2
    pcl::PointCloud<pcl::PointXYZ> cloudl1;
    pcl::PointCloud<pcl::PointXYZ> cloudl2;

    //Bool SIgnal, ob das Turtlebot sich an der linken Seite von der Grenzlinie befindet
    bool leftoflinefirst = false;
    bool leftoflinesecond = false;
    bool checktwolines_s = false;
    bool checktheta = false;
    bool checktrue = true;

    //Parameter der Grenzlinie
    std_msgs::Float64MultiArray line1_parameters_s;
    std_msgs::Float64MultiArray line2_parameters_s;

    //Lidar Signale verarbeiten
    void processCloud(const sensor_msgs::PointCloudConstPtr &var);

    //MSG zum Steuer-Node schicken
    bool initial_Locsrv (plane_segmentation::initial_Loc::Request &req, plane_segmentation::initial_Loc::Response &res);

    //MSG zum Lokalization-Node schicken
    bool next_Locsrv (plane_segmentation::next_Loc::Request &req, plane_segmentation::next_Loc::Response &res);

    public:
    LineSegmentation(ros::NodeHandle nh) : nh_(nh), priv_nh_("~") //,
    {

        sub_pc = nh_.subscribe("/lidarscan_modified", 10, &LineSegmentation::processCloud, this);

        pub_line1 = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/line1_points", 10);
        pub_line2 = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/line2_points", 10);
        pub_pointmean = nh_.advertise<pcl::PointCloud<pcl::PointXYZ>>("/segmentation/pointmean",10);

        initial_Loc=nh_.advertiseService("initial_Loc", &LineSegmentation::initial_Locsrv,this);
        next_Loc=nh_.advertiseService("next_Loc",&LineSegmentation::next_Locsrv,this );
    }

    ~LineSegmentation() {}
};

//Abstand zweier Punkten rechnen
inline double CalculateDistance( pcl::PointXYZ pointCore, pcl::PointXYZ pointTarget )
{
    return pow(pointCore.x - pointTarget.x,2)+pow(pointCore.y - pointTarget.y,2);
}

//Abstand zwischen Grenzlinie und dem Turtelbot
inline float CalculateX(float a, float b, float c)
{
    return abs(c/sqrt(pow(a,2)+pow(b,2)));
}

//den weitesten Punkt auf der Grenzlinie rechnen
pcl::PointXYZ Farestpoint (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    vector<double> cloud_dis;
    int length = cloud->points.size();

    for(int i = 0; i < cloud->points.size(); i++)
    {
      pcl::PointXYZ point1=cloud->points[i];
      pcl::PointXYZ point2;
      point2.x=0;
      point2.y=0;
      point2.z=0;
      double dis=CalculateDistance(point1, point2);
      cloud_dis.push_back(dis);
    }

    //int index=max_element(cloud_dis[0], cloud_dis[length-1]);
    int index_f= std::max_element(cloud_dis.begin(),cloud_dis.end()) - cloud_dis.begin();
    return cloud->points[index_f];
}

//den nahesten Punkt der Grenzlinie rechnen
pcl::PointXYZ Nearestpoint (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    vector<double> cloud_dis;
    int length = cloud->points.size();

    for(int i = 0; i < length; i++)
    {
      pcl::PointXYZ point1=cloud->points[i];
      pcl::PointXYZ point2;
      point2.x=0;
      point2.y=0;
      point2.z=0;
      double dis=CalculateDistance(point1, point2);
      cloud_dis.push_back(dis);
    }

    int index_n = std::min_element(cloud_dis.begin(),cloud_dis.end()) - cloud_dis.begin();
    return cloud->points[index_n];
}

//Kreuzprodukt fuer Links/Rechts Ueberpruefung
inline float Crossproduct(pcl::PointXYZ p, pcl::PointXYZ q)
{
    return q.x*p.y - q.y*p.x;
}

//Links/Rechts Ueberpruefung
bool LeftOfLine(pcl::PointXYZ p, pcl::PointXYZ q)
{
    if (Crossproduct(p,q) > 0){
      return true;
    }
    else{
      return false;
    }
}

//Winkel, mit dem das Turtelbot sich ins Mittel des Spielsfelds umdrehen lassen
float Calculateangle(pcl::PointXYZ p, pcl::PointXYZ q)
{
    float dy = p.y - q.y;
    float dx = p.x - q.x;
    float angletemp = atan(abs(dy)/abs(dx));
    float angle;

    bool left = LeftOfLine(p,q);

    if ( !LeftOfLine(p,q) ){
        if ( dy < 0 && dx <0 )
        {
          angle = 1.5*M_PI + angletemp;
        }
        else if ( dy >0 && dx <0 )
        {
          angle = 1.5*M_PI - angletemp;
        }
        else if ( dy >0 && dx >0 )
        {
          angle = 0.5*M_PI + angletemp;
        }
        else
        {
          angle = 0.5*M_PI - angletemp;
        }
    }
    else{
        if (dy <0 && dx <0)
        {
          angle = 0.5*M_PI + angletemp;
        }
        else if ( dy >0 && dx <0 )
        {
          angle = 0.5*M_PI - angletemp;
        }
        else if (dy >0 && dx >0 )
        {
          angle = 1.5*M_PI + angletemp;
        }
        else
        {
          angle = 1.5*M_PI - angletemp;
        }
    }

    if ( angle > M_PI)
    {
      angle = angle - 2*M_PI;
    }

    return angle;

}

// Koordianten Umstellung fuer ein punkt um Ausserreisser zu filtern
pcl::PointXYZ coordinateconversion (pcl::PointXYZ pt, float theta)
{
  pcl::PointXYZ p;
  p.x = pt.x*cos(theta) + pt.y*sin(theta);
  p.y = pt.y*cos(theta) - pt.x*sin(theta);
  p.z = 0;

  return p;
}

// Koordianten Umstellung fuer ein point cloud um Ausserreisser zu filtern
pcl::PointCloud<pcl::PointXYZ>::Ptr Pcoordinateconversion (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, float theta)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr c(new pcl::PointCloud<pcl::PointXYZ>);
  c->header = cloud->header;
  for (auto i:cloud->points){
    c->push_back(coordinateconversion(i,theta));
  }
  return c;
}

//Propotion Pruefung
pcl::PointCloud<pcl::PointXYZ>::Ptr Portioncheck (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1,pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr pt (new pcl::PointCloud<pcl::PointXYZ>);
  pt->header=cloud1->header;
  for (auto i:cloud1->points){
    for (auto j:cloud2->points){
      if (abs(i.y-j.y)<biascoor){
        pt->push_back(i);
        break;
      }
    }
  }
  return pt;
}

//Centriod Punkt rechnen
pcl::PointCloud<pcl::PointXYZ>::Ptr MeanCluster(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_mean (new pcl::PointCloud<pcl::PointXYZ> );
  cloud_mean->header = cloud->header;
  bool loop=true;

  while (loop)
  {
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    pcl::CentroidPoint<pcl::PointXYZ> centroid;

    int ctime=0;
    for (int j=0; j<cloud->points.size();j++){

      if (CalculateDistance(cloud->points[0],cloud->points[j]) < EPSILON) // e.g. remove all pts below zAvg
      {
        inliers->indices.push_back(j);
        centroid.add(cloud->points[j]);
        ctime++;
      }
    }
    pcl::PointXYZ c1;
    centroid.get (c1);
    cloud_mean->push_back(c1);

    extract.setInputCloud(cloud);
    extract.setIndices(inliers);
    extract.setNegative(true);
    extract.filter(*cloud);

    if (cloud->points.size()==0){
      loop=false;
    }
  }

  return cloud_mean;
}

// Pruefung von einer Grenzlinie
bool onelinecheck (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_1)
{
    vector<double> cloud_line1_dis;
    int times = 0;
    bool check = false;
    bool check13 = false;
    bool check12 = false;

    //ROS_INFO_STREAM("cloud line 1 size "<<cloud_line_1->points.size());
    if (cloud_line_1->points.size() >1)
    {
      for(int i = 0; i < cloud_line_1->points.size()-1; i++)
      {
        pcl::PointXYZ point1=cloud_line_1->points[i];
        pcl::PointXYZ point2=cloud_line_1->points[i+1];
        double dis1=CalculateDistance(point1, point2);
        cloud_line1_dis.push_back(dis1);
      }
    }

    for (auto dis1 : cloud_line1_dis){

        for (auto dis2 : cloud_line1_dis){
            //Proportion 1:1
            if (abs(dis1-dis2) < bias){
              times++;
            }
            //Proportion 1:3
            if (abs(dis1/dis2 - 0.333) < bias13){
              check13 = true;
            }
            //Proportion 1:2
            if (abs(dis1/dis2 - 0.5) < bias12){
              check12 = true;
              }
        }
    }
    if ((times >1) && check12 && check13){
        check =true;
    }

    return check;
}

//Pruefung der zweinen Grenzlinien
bool twolinescheck (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_1, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_2)
{
  vector<double> cloud_line1_dis;
  vector<double> cloud_line2_dis;
  bool check=false;
  int time =0;

  if (cloud_line_1->points.size() >1)
  {
    for(int i = 0; i < cloud_line_1->points.size()-1; i++)
    {
      pcl::PointXYZ point1=cloud_line_1->points[i];
      pcl::PointXYZ point2=cloud_line_1->points[i+1];
      double dis1=CalculateDistance(point1, point2);
      cloud_line1_dis.push_back(dis1);
    }
  }

  if (cloud_line_2->points.size() >1)
  {
    for(int i = 0; i < cloud_line_2->points.size()-1; i++)
    {
      pcl::PointXYZ point1=cloud_line_2->points[i];
      pcl::PointXYZ point2=cloud_line_2->points[i+1];
      double dis2=CalculateDistance(point1, point2);
      cloud_line2_dis.push_back(dis2);
    }
  }

  if (!cloud_line1_dis.empty() && !cloud_line2_dis.empty())
  {
    for (vector<double>::iterator it = cloud_line1_dis.begin(); it != cloud_line1_dis.end(); ++it)
    {
      for (vector<double>::iterator it2 = cloud_line2_dis.begin(); it2 != cloud_line2_dis.end(); ++it2)
      {
        if (double dis=abs(*it-*it2) < bias){
          time ++;

        }
      //}
        if (time ==1 && cloud_line_1->points.size() > 2 && cloud_line_2->points.size() > 2){
          check = true;
          time = 0;
          break;
          ROS_INFO("checktwoline in fc true");

        }
      }
    }
  }
  return check;
}

//Abstand rechnen
float distance(Eigen::Vector4f v1, Eigen::Vector4f v2)
{
    float dis_ = ((v2[0]-v1[0])*v1[3]/v1[2] + (v1[1]-v2[1])) * v1[2]/(sqrt(v1[2]*v1[2]+v1[3]*v1[3]));
    if (dis_ <= 0)
    {
        dis_ = -dis_;
    }
    return dis_;
}

//LIDAR Signal Verarbeitung
void LineSegmentation::processCloud(const sensor_msgs::PointCloudConstPtr& input)
{
    //Umwandlung von ROS_MSG zum PCL Datentyp
    sensor_msgs::PointCloud2 cloud2;
    sensor_msgs::convertPointCloudToPointCloud2(*input, cloud2);
    pcl::PointCloud< pcl::PointXYZ > pc;
    pcl::fromROSMsg(cloud2, pc);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1 = pc.makeShared(); // cloud to operate
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = MeanCluster(cloud1);

    pub_pointmean.publish(cloud);

    pcl::VoxelGrid<pcl::PointXYZ> sor;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered( new pcl::PointCloud<pcl::PointXYZ> );
    sor.setInputCloud(cloud);
    sor.setLeafSize(0.01f, 0.01f, 0.01f);
    sor.filter(*cloud_filtered);   // cloud_filtered is the result after downsampleing

    pcl::SACSegmentation<pcl::PointXYZ> seg_1, seg_2;
    pcl::PointIndices::Ptr inliers( new pcl::PointIndices );
    pcl::PointIndices::Ptr inliers_2( new pcl::PointIndices );
    pcl::ModelCoefficients::Ptr coefficients_1( new pcl::ModelCoefficients );
    pcl::ModelCoefficients::Ptr coefficients_2( new pcl::ModelCoefficients );
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_1( new pcl::PointCloud<pcl::PointXYZ>() );
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_2( new pcl::PointCloud<pcl::PointXYZ>() );

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_clusters( new pcl::PointCloud<pcl::PointXYZ>() );

    seg_1.setOptimizeCoefficients(true);
    seg_1.setModelType(pcl::SACMODEL_LINE);// SACMODEL_PLANE);//use the determine plane model for SAC
    seg_1.setMethodType(pcl::SAC_RANSAC);//sample method
    seg_1.setMaxIterations(200);// maximal 100 times iteration
    seg_1.setDistanceThreshold(0.04);

    seg_1.setInputCloud(cloud_filtered);
    seg_1.segment(*inliers, *coefficients_1);
    Eigen::Vector3f axis;
    axis << coefficients_1->values[4], -coefficients_1->values[3], coefficients_1->values[5];

    //Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZ> extract, extract_2;
    extract.setInputCloud(cloud_filtered);
    extract.setIndices(inliers);
    extract.setNegative(false);

    extract.filter(*cloud_line_1);

    //cloud_line_1=MeanCluster(cloud_line_1);

    //remove the lines' inliers, extract the rest
    extract.setNegative(true);
    extract.filter(*cloud_clusters);

    cloud_clusters = MeanCluster(cloud_clusters);
    // find the second line based on the coefficient of the first line
    seg_2.setOptimizeCoefficients(true);
    seg_2.setMethodType(pcl::SAC_RANSAC);
    seg_2.setModelType(pcl::SACMODEL_PARALLEL_LINE);
    //seg_2.setMaxIterations(100);
    seg_2.setMaxIterations(200);
    seg_2.setDistanceThreshold(0.1);
    seg_2.setAxis(axis);
    //seg_2.setEpsAngle(pcl::deg2rad(2.0));
    seg_2.setEpsAngle(pcl::deg2rad(4.0));
    seg_2.setInputCloud(cloud_clusters);
    seg_2.segment(*inliers_2, *coefficients_2);

    extract_2.setInputCloud(cloud_clusters);
    extract_2.setIndices(inliers_2);
    extract_2.setNegative(false);
    extract_2.filter(*cloud_line_2);

    //cloud_line_2=MeanCluster(cloud_line_2);

    Eigen::Vector4f paramrter_1, paramrter_2;
    std_msgs::Float64MultiArray line1_parameters;
    std_msgs::Float64MultiArray line2_parameters;
    paramrter_1 << coefficients_1->values[0], coefficients_1->values[1], coefficients_1->values[3], coefficients_1->values[4];
    line1_parameters.data = {coefficients_1->values[0], coefficients_1->values[1], coefficients_1->values[3], coefficients_1->values[4]};
    paramrter_2 << coefficients_2->values[0], coefficients_2->values[1], coefficients_2->values[3], coefficients_2->values[4];
    line2_parameters.data = {coefficients_2->values[0], coefficients_2->values[1], coefficients_2->values[3], coefficients_2->values[4]};

    float dis1 = distance(paramrter_1, paramrter_2);

    float a = paramrter_1[3]/paramrter_1[2];
    float b = -1;
    float c = -(paramrter_1[3]/paramrter_1[2])*paramrter_1[0]+paramrter_1[1];
    float x = CalculateX(a,b,c);

    if (cloud_line_1->points.size() >0){
      pcl::PointXYZ p = Farestpoint (cloud_line_1);
      pcl::PointXYZ q = Nearestpoint (cloud_line_1);

      int s = cloud_line_1->points.size();
      //check teh theta is stable
      float angle = Calculateangle(p,q);
      //save angle history
      thetavector[indextheta] = angle;
      indextheta ++;
      if (indextheta == 2){
        indextheta = 0;
      }
      //for (auto i : thetavector)
      //{
      //  ROS_INFO_STREAM("theta vector "<< i);
      //}

      //check consistent value of theta

      //if (abs(thetavector[0]-thetavector[1]) < thetabias && abs(thetavector[1]-thetavector[2]) < thetabias){
      if (abs(thetavector[0] - thetavector[1]) < thetabias){
        checktheta = true;
      }
      else
      {
        checktheta = false;
      }

      bool leftofline = LeftOfLine(p,q);

      if ((twolinescheck (cloud_line_1, cloud_line_2) == true && dis1 > disconst)){
          times_twoline++;
          //ROS_INFO("twolines check nr %d",times_twoline);
      }

      //ROS_INFO_STREAM("checktheta " << checktheta);
      //ROS_INFO_STREAM("onelinecheck "<< onelinecheck(cloud_line_1));

      pub_line1.publish(cloud_line_1);
          //pub_line1_parameters.publish(line1_parameters);
      pub_line2.publish(cloud_line_2);

      bool checktl_s = twolinescheck(cloud_line_1,cloud_line_2) && (dis1 > disconst) ;

      if(onelinecheck(cloud_line_1))
      {


          if (checktheta)
          {   angle_ros.data = angle;
              x_ros.data = x;
              ROS_INFO("one line check ok");
          }

          if (times_twoline >= 1)
          {

              if (checktl_s)
              {
                  // Ausserreisser ausfiltern
                  if (cloud_line_1->points.size() > 1 && cloud_line_2->points.size()>1)
                  {
                      pcl::PointCloud<pcl::PointXYZ>::Ptr cl1 (new pcl::PointCloud<pcl::PointXYZ>);
                      pcl::PointCloud<pcl::PointXYZ>::Ptr cl2 (new pcl::PointCloud<pcl::PointXYZ>);
                      cl1=Pcoordinateconversion(cloud_line_1,angle);
                      cl2=Pcoordinateconversion(cloud_line_2,angle);

                      cloud_line_1=Portioncheck(cl1,cl2);
                      cloud_line_2=Portioncheck(cl2,cl1);
                      float angleback = M_PI*2-angle;
                      cloud_line_1=Pcoordinateconversion(cloud_line_1,angleback);
                      cloud_line_2=Pcoordinateconversion(cloud_line_2,angleback);
                  }


                  bool check_s = checktl_s && (cloud_line_1->points.size()>2) && (cloud_line_2->points.size()>2) ;
                  //ROS_INFO_STREAM("<<<TWO LINES CHECK "<< check_s);
                //  ROS_INFO_STREAM("iniital_loccheck"<<initial_loccheck);
              if (initial_loccheck && checktrue)
              {    if (check_s)
                  {
                    cloudl1=*cloud_line_1;
                    cloudl2=*cloud_line_2;

                    leftoflinefirst = leftofline;
                    leftoflinesecond = !leftofline;

                    line1_parameters_s = line1_parameters;
                    line2_parameters_s = line2_parameters;
                  }
                   if ( abs(angle) < anglebias || abs(angle-0.5*M_PI) < anglebias || abs(angle-M_PI) < anglebias || abs(angle+M_PI) < anglebias || abs(angle+0.5*M_PI) <anglebias)
                   {
                     checktwolines_s = false;
                   }
                   else if (check_s)
                   {
                      checktwolines_s = true;
                      checktrue =false;
                      ROS_INFO("<<< in two lines check");
                   }
                   else
                   {
                      checktwolines_s = false;
                   }
                }

                //check_s.data = checktwolines_s;
              }
          }
      }
    }

}

bool LineSegmentation::initial_Locsrv (plane_segmentation::initial_Loc::Request &req, plane_segmentation::initial_Loc::Response &res)
{
  //for service
  if (req.localization.data){
    res.dis = x_ros;
    res.theta = angle_ros;
    std_msgs::Bool check_s;
    check_s.data = checktwolines_s;
    //ROS_INFO_STREAM(" checktwolines_s" <<  checktwolines_s);
    res.lines = check_s;
    //res.lines = checktwolines_s
    //ROS_INFO_STREAM(" check_s " << res.lines);
    initial_loccheck =true;
  }
  else{
    initial_loccheck = false;
  }
  return true;
}

bool LineSegmentation::next_Locsrv (plane_segmentation::next_Loc::Request &req, plane_segmentation::next_Loc::Response &res)
{
  if (req.start.data){
    std_msgs::Bool leftofl1;
    leftofl1.data = leftoflinefirst;
    res.leftofline1 = leftofl1;

    std_msgs::Bool leftofl2;
    leftofl2.data = leftoflinesecond;
    res.leftofline2 = leftofl2;

    res.line1_parameters = line1_parameters_s;
    res.line2_parameters = line2_parameters_s;

    std_msgs::Bool checknext;
    checknext.data = checktwolines_s;
    res.checktwolines = checknext;

    sensor_msgs::PointCloud2 l1;
    pcl::toROSMsg(cloudl1,l1);
    res.line1 = l1;

    sensor_msgs::PointCloud2 l2;
    pcl::toROSMsg (cloudl2,l2);
    res.line2 = l2;

  }
  return true;

}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "line_segmentation");
    ros::NodeHandle nh;
    LineSegmentation node(nh);

    ros::spin();
    return 0;
}
