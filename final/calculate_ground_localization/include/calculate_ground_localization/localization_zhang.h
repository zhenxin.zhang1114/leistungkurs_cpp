//#ifdef MYHEAD_FILE
//#define MYHEAD_FILE
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Pose.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/io.h>
#include <pcl/visualization/cloud_viewer.h>
#include <nav_msgs/Odometry.h>
#include <boost/tuple/tuple.hpp>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/impl/io.hpp>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <Eigen/Dense>
#include <calculate_ground_localization/ground_info.h>
#include <std_msgs/Float64MultiArray.h>
using namespace std;
using namespace Eigen;

class calculate_groundClass
{

private:
  //! The node handle
  ros::NodeHandle nh_;
  //! Node handle in the private namespace
  ros::NodeHandle priv_nh_;
  //! Subscribers to the PointCloud data
  ros::Subscriber sub1; //Subscriber of line1
  ros::Subscriber sub2; //Subscriber of line2

  ros::Subscriber line1_parameters_sub; //Subscriber of line1
  ros::Subscriber line2_parameters_sub; //Subscriber of line2
  ros::Subscriber check_sub;

  ros::Subscriber line_angle_sub; //Subscriber of angle between turtle and line
  ros::Subscriber turtle_line1_side_sub;
  ros::Subscriber turtle_line2_side_sub;

  //! Publisher for pointclouds
  ros::Publisher pub_line1; //Publisher of the dealed line info
  ros::Publisher pub_line2;
  //server to return the info of ground
  ros::ServiceServer  info_server; //serverto send message to Hongxu
  //-----------Variables------
  //pcl type pcl,use to transfer ros::pcl to pcl::pcl
  pcl::PointCloud<pcl::PointXYZ> pc_line1_filtered;
  pcl::PointCloud<pcl::PointXYZ> pc_line2_filtered;
  //transfer pcl::pcl to ros::pcl
  sensor_msgs::PointCloud2 output2_1;
  sensor_msgs::PointCloud2 output2_2;
  //dis from turtle to line
  float turtle2line1Dis;
  float turtle2line2Dis;
  //result of a calculate using each line
  float a_line1;
  float a_line2;
  bool check;
  float Final_a;
  float Final_b;
  float turtle_x;
  float turtle_y;

  float angle_turtlewithline;
  bool line1_leftside;
  bool line2_leftside;
  int Line1PtsCase;
  int Line2PtsCase;

  //parameters of the detected line, x0=parameters[0],y0=parameter[1],a=parameter[2],b=parameter[3]
  Eigen::Vector4f parameter_1;
  Eigen::Vector4f parameter_2;




  //------------------functions----------
  float calculate_dis(float x_,float y_);
  float calculate_b(Eigen::Vector4f input1,Eigen::Vector4f input2);
  float get_dis2line(Eigen::Vector4f input);
  std::vector<Eigen::Vector3f> transfer2Matrix(pcl::PointCloud<pcl::PointXYZ> input);
  std::vector<Eigen::Vector3f> returnDisof2Pt(pcl::PointCloud<pcl::PointXYZ> input);
  std::vector<Eigen::Vector3f> sort_disinTurn(pcl::PointCloud<pcl::PointXYZ> input);
  std::vector<Eigen::Vector4f> indexthePC(pcl::PointCloud<pcl::PointXYZ> input,Eigen::Vector4f parameter,bool leftofline);
  Eigen::Vector2f restoredPt(Eigen::Vector4f parameters,bool leftofline,float X_in,float Y_in,float delta_Y, float delta_X,float dis,bool up);


  //help functions
  Eigen::Vector3f newPt2setedPts(std::vector<Eigen::Vector3f>input1,pcl::PointCloud<pcl::PointXYZ> input2,Eigen::Vector3f input3);
  Eigen::Vector3f get2nearstPts(std::vector<Eigen::Vector3f> input);
  int return_idx_in_matrix(std::vector<Eigen::Vector3f> input,int idx);
  int find_same_index(Eigen::Vector3f input1,Eigen::Vector3f input2);
  int find_different_indexwithInt(Eigen::Vector3f input1,int input2);
  int find_same_indexwithInt(Eigen::Vector3f input1,int input2);
  int findminColIdxinVec(std::vector<Eigen::Vector4f> resortedpc_info);
  std::vector<Eigen::Vector4f> sortColIdx(std::vector<Eigen::Vector4f> resortedpc_info);

  std::vector<float> reletive_position_info(Eigen::Vector4f parameters);
  bool calculate_Position_callback(calculate_ground_localization::ground_info::Request &req,calculate_ground_localization::ground_info::Response &res);

  Eigen::Vector2f restoredPt(Eigen::Vector4f parameters,float Xin,float Yin,float dis,bool flag,int col1index);
  Eigen::Vector2f restoreConerColumn(Eigen::Vector3f columnCoordinate,int PositionCase);
  Eigen::Vector2f getHelpPoint(Eigen::Vector4f parameters);
  //------------------ Callbacks -------------------
  void line1_callback(const sensor_msgs::PointCloud2ConstPtr& input);
  void line2_callback(const sensor_msgs::PointCloud2ConstPtr& input);
  void line1_para_callback(const std_msgs::Float64MultiArray::ConstPtr& input)
  {
    ROS_INFO_STREAM("size: " << input->data.size());
    ROS_INFO_STREAM("empty: " << input->data.empty());
    ROS_INFO_STREAM(__LINE__);
    if(input->data[0]!= 0 && input->data[1]!= 0 && input->data[2]!= 0 && input->data[3]!= 0)
    {
      parameter_1[0]=input->data[0],parameter_1[1]=input->data[1],parameter_1[2]=input->data[2],parameter_1[3]=input->data[3];
    }
    ROS_INFO_STREAM(__LINE__);

  }
  void line2_para_callback(const std_msgs::Float64MultiArray::ConstPtr& input)
  {
    ROS_INFO_STREAM("size: " << input->data.size());
    ROS_INFO_STREAM("empty: " << input->data.empty());
    ROS_INFO_STREAM(__LINE__);
    if(input->data[0]!= 0 && input->data[1]!= 0 && input->data[2]!= 0 && input->data[3]!= 0)
    {
      parameter_2[0]=input->data[0],parameter_2[1]=input->data[1],parameter_2[2]=input->data[2],parameter_2[3]=input->data[3];
    }
    ROS_INFO_STREAM(__LINE__);
  }
  void check2points_callback(const std_msgs::BoolConstPtr& input){check = input->data;ROS_INFO("check points callback");}
  void angle_callback(const std_msgs::Float64ConstPtr& input){angle_turtlewithline = input->data;cout<<angle_turtlewithline;ROS_INFO("angle callback");}
  //bool calculate_Position_callback(calculate_ground_localization::ground_info::Request &req,calculate_ground_localization::ground_info::Response &res);
  void side_callback_line1(const std_msgs::BoolConstPtr& input){line1_leftside = input->data;}
  void side_callback_line2(const std_msgs::BoolConstPtr& input){line2_leftside = input->data;}

  Eigen::Vector2f returnThetaSide(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameters);
  Eigen::Vector2f returnInitialXY(Eigen::Vector4f conerXY,Eigen::Vector4f parameters);

public:
  calculate_groundClass(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
  {
    ROS_INFO("int the node");
    //pub_line1 = nh_.advertise<sensor_msgs::PointCloud2>("/segmentation/line1_test", 10);
    //pub_line2 = nh_.advertise<sensor_msgs::PointCloud2>("/segmentation/line2_test", 10);
    info_server = nh_.advertiseService("InitialPosition",&calculate_groundClass::calculate_Position_callback,this);
    sub1 = nh_.subscribe("/segmentation/line1_points", 10, &calculate_groundClass::line1_callback ,this);
    ROS_INFO("after line1 sub");
    sub2 = nh_.subscribe("/segmentation/line2_points", 10, &calculate_groundClass::line2_callback,this);
    ROS_INFO("after line2 sub");
    line_angle_sub = nh_.subscribe("/segmentation/angle", 10, &calculate_groundClass::angle_callback,this);
    ROS_INFO("after angle sub");
    turtle_line1_side_sub = nh_.subscribe("/segmentation/leftofline1", 10, &calculate_groundClass::side_callback_line1,this);
    turtle_line2_side_sub = nh_.subscribe("/segmentation/leftofline2", 10, &calculate_groundClass::side_callback_line2,this);
    line1_parameters_sub = nh_.subscribe("/segmentation/line1_parameters",10,&calculate_groundClass::line1_para_callback,this);
    line2_parameters_sub = nh_.subscribe("/segmentation/line2_parameters",10,&calculate_groundClass::line2_para_callback,this);
    check_sub = nh_.subscribe("/segmentation/checktwolines",10,&calculate_groundClass::check2points_callback,this);
  }
  ~calculate_groundClass() {}
};
