//#ifdef MYHEAD_FILE
//#define MYHEAD_FILE
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Pose.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/io.h>
#include <pcl/visualization/cloud_viewer.h>
#include <nav_msgs/Odometry.h>
#include <boost/tuple/tuple.hpp>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/impl/io.hpp>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <Eigen/Dense>
#include <calculate_ground_localization/ground_info.h>
#include <calculate_ground_localization/next_Loc.h>

#include <std_msgs/Float64MultiArray.h>
using namespace std;
using namespace Eigen;

class calculate_groundClass
{

private:
    //! The node handle
    ros::NodeHandle nh_;
    ros::NodeHandle priv_nh_;
    ros::ServiceClient pts_infoclient;
    ros::ServiceServer  info_server; //serverto send message to Hongxu
    ros::Publisher reseted_pt_pub_1;
    ros::Publisher original_pc_pub_1;
    ros::Publisher reseted_pt_pub_2;
    ros::Publisher original_pc_pub_2;

    //-----------Variables------
    //pcl type pcl,use to transfer ros::pcl to pcl::pcl
    pcl::PointCloud<pcl::PointXYZ> pc_line1_filtered;
    pcl::PointCloud<pcl::PointXYZ> pc_line2_filtered;

    //transfer pcl::pcl to ros::pcl
    sensor_msgs::PointCloud2 input_line1;
    sensor_msgs::PointCloud2 input_line2;
    bool line1_leftside;
    bool line2_leftside;
    //dis from turtle to line
    float turtle2line1Dis;
    float turtle2line2Dis;
    Eigen::Vector4f parameter_1;
    Eigen::Vector4f parameter_2;
    //result of a calculate using each line
    float a_line1;
    float a_line2;
    bool check;
    float Final_a;
    float Final_b;
    float Initial_theta;
    string line_side;
    int Line1PtsCase;
    int Line2PtsCase;
    calculate_ground_localization::next_Loc gd_info_srv;
    bool calculate_Position_callback(calculate_ground_localization::ground_info::Request &req,calculate_ground_localization::ground_info::Response &res);


    //------------------functions----------
    float calculate_dis(float x_,float y_);
    float calculate_b(Eigen::Vector4f input1,Eigen::Vector4f input2);
    float get_dis2line(Eigen::Vector4f input);
    std::vector<Eigen::Vector3f> transfer2Matrix(pcl::PointCloud<pcl::PointXYZ> input);
    std::vector<Eigen::Vector3f> returnDisof2Pt(pcl::PointCloud<pcl::PointXYZ> input);
    std::vector<Eigen::Vector3f> sort_disinTurn(pcl::PointCloud<pcl::PointXYZ> input);
    std::vector<Eigen::Vector4f> indexthePC(pcl::PointCloud<pcl::PointXYZ> input,Eigen::Vector4f parameter);
    std::vector<Eigen::Vector4f> restoreLine(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameter);

    Eigen::Vector2f restoredPt(Eigen::Vector4f parameters,bool leftofline,float X_in,float Y_in,float delta_Y, float delta_X,float dis,bool up);
    Eigen::Vector2f get_theta(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameters);

    //help functions
    Eigen::Vector3f newPt2setedPts(std::vector<Eigen::Vector3f>input1,pcl::PointCloud<pcl::PointXYZ> input2,Eigen::Vector3f input3);
    Eigen::Vector3f get2nearstPts(std::vector<Eigen::Vector3f> input);
    int return_idx_in_matrix(std::vector<Eigen::Vector3f> input,int idx);
    int find_same_index(Eigen::Vector3f input1,Eigen::Vector3f input2);
    int find_different_indexwithInt(Eigen::Vector3f input1,int input2);
    int find_same_indexwithInt(Eigen::Vector3f input1,int input2);
    int findminColIdxinVec(std::vector<Eigen::Vector4f> resortedpc_info);
    std::vector<Eigen::Vector4f> sortColIdx(std::vector<Eigen::Vector4f> resortedpc_info);
    Eigen::Vector2f restoredPt(Eigen::Vector4f parameters,float Xin,float Yin,float dis,bool flag,int col1index);
    Eigen::Vector2f restoreConerColumn(Eigen::Vector3f columnCoordinate,int PositionCase);
    Eigen::Vector2f getHelpPoint(Eigen::Vector4f parameters);
    pcl::PointCloud<pcl::PointXYZ> makepc(std::vector<Eigen::Vector4f> newPc_coordinate,pcl::PointCloud<pcl::PointXYZ> original_pc);
    Eigen::Vector2f getpointinline(Eigen::Vector4f parameters,float x_0,float y_0);

    //------------------ Callbacks -------------------

    Eigen::Vector2f returnThetaSide(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameters,bool left_of_line);
    Eigen::Vector2f returnInitialXY(Eigen::Vector4f conerXY,Eigen::Vector4f parameters);

public:
    calculate_groundClass(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
    {
      info_server = nh_.advertiseService("InitialPosition",&calculate_groundClass::calculate_Position_callback,this);
      pts_infoclient = nh_.serviceClient<calculate_ground_localization::next_Loc>("next_Loc");

      reseted_pt_pub_1 = nh_.advertise<sensor_msgs::PointCloud2>("Segmentation/reseted_pts1",1000);
      original_pc_pub_1 = nh_.advertise<sensor_msgs::PointCloud2>("Segmentation/original_pts1",1000);
      reseted_pt_pub_2 = nh_.advertise<sensor_msgs::PointCloud2>("Segmentation/reseted_pts2",1000);
      original_pc_pub_2 = nh_.advertise<sensor_msgs::PointCloud2>("Segmentation/original_pts2",1000);
    }
    ~calculate_groundClass() {}
};
