#include "calculate_ground_localization/localization_zhang.h"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "ground_info_localization_server");
    ros::NodeHandle nh;
    calculate_groundClass node(nh);
    ros::spin();
    return 0;
}
