#include "calculate_ground_localization/localization_zhang.h"

bool calculate_groundClass::calculate_Position_callback(calculate_ground_localization::ground_info::Request &req,calculate_ground_localization::ground_info::Response &res){
  if(req.start == true && parameter_1[0] != 0 && parameter_2[3] != 0 )
  {
    Final_b = calculate_b(parameter_1,parameter_2);
    std::vector<Eigen::Vector4f> line1_resortedpc_info = indexthePC(pc_line1_filtered,parameter_1,line1_leftside);
    Eigen::Vector4f conerXY_line1 = line1_resortedpc_info[1];
    cout<<"coner:"<<conerXY_line1[0]<<conerXY_line1[1]<<conerXY_line1[2]<<conerXY_line1[3]<<endl;
    Eigen::Vector2f InitialXY_line1 = returnInitialXY(conerXY_line1,parameter_1);
    //std::vector<Eigen::Vector4f> line2_resortedpc_info = indexthePC(pc_line2_filtered,parameter_2,line2_leftside);
    //Eigen::Vector4f conerXY_line2 = line2_resortedpc_info[1];
    //Eigen::Vector2f InitialXY_line2 = returnInitialXY(conerXY_line2,parameter_2);
    Final_a = line1_resortedpc_info[0][1];
    Eigen::Vector2f thetaSide_line1=returnThetaSide(line1_resortedpc_info,parameter_1);
    string line1_side;
    if(thetaSide_line1[1]){
      line1_side = "left";
    }
    else{
      line1_side = "right";
    }
    //Eigen::Vector2f thetaSide_line2=returnThetaSide(line1_resortedpc_info,parameter_1);
    ROS_INFO("received request");
    res.a = Final_a;
    res.b = Final_b;
    res.theta=thetaSide_line1[0];
    res.x_ = InitialXY_line1[0];
    res.y_ = InitialXY_line1[1];
    res.place = line1_side;

  }
  else if (req.start == true)
  {
    res.a = 0;
    res.b = 0;
    res.theta=0;
    res.x_ = 0;
    res.y_ = 0;
    res.place = "unknown";
  }
  return true;
}

void calculate_groundClass::line1_callback(const sensor_msgs::PointCloud2ConstPtr& input){
  ROS_INFO("*******ground infomation*********");
  pcl::fromROSMsg(*input, pc_line1_filtered);//pc_line1 type:pcl::PointCloud<pcl::PointXYZ>
}


void calculate_groundClass::line2_callback(const sensor_msgs::PointCloud2ConstPtr& input){
  pcl::fromROSMsg(*input, pc_line2_filtered);//pc_line1 type:pcl::PointCloud<pcl::PointXYZ>
}

float calculate_groundClass::calculate_dis(float x_, float y_){
  float distans = sqrt(pow(x_,2)+pow(y_,2));
  return distans;
}

float calculate_groundClass::get_dis2line(Eigen::Vector4f input){
  float x0=input[0];
  float y0=input[1];
  float a=input[2];
  float b=input[3];
  float dis = abs((a*y0-b*x0)/sqrt(a*a+b*b));
  return dis;
}

float calculate_groundClass::calculate_b(Eigen::Vector4f input1,Eigen::Vector4f input2){
  float b;
  b = get_dis2line(input1)+get_dis2line(input2);
  return b;
}

std::vector<Eigen::Vector3f> calculate_groundClass::transfer2Matrix(pcl::PointCloud<pcl::PointXYZ> input){ //schon vermeiden specher fehler
  int numpc=input.size();
  std::vector<Eigen::Vector3f> pcCoordinateWithIndex;
  if(input.size()!=0){ //  vermeiden speicher fehler
    for(int i=0;i<numpc;i++){
      Eigen::Vector3f tmp;
      tmp<<i,input.points[i].x,input.points[i].y;
      pcCoordinateWithIndex.push_back(tmp);
    }
  }
  else{
    Eigen::Vector3f tmp;
    tmp<<0,0,0;
    pcCoordinateWithIndex.push_back(tmp);
  }
  return pcCoordinateWithIndex;
}

Eigen::Vector3f calculate_groundClass::newPt2setedPts(std::vector<Eigen::Vector3f> input1,pcl::PointCloud<pcl::PointXYZ> input2,Eigen::Vector3f input3){
  float min_dis = 1000;
  float tmp_dis;
  int idx_inVec;
  Eigen::Vector3f Pt2setedPts;
  int idx_inVec1;
  int idx_inVec2;
  for(int i=0;i<input1.size();i++){
    for(int j=0;j<2;j++){
      tmp_dis = calculate_dis(input2.points[input1[i][j]].x-input3[1],input2.points[input1[i][j]].y-input3[2]);
      if(tmp_dis<min_dis){
        min_dis = tmp_dis;
        idx_inVec1 = i;
        idx_inVec2 = j;
      }
    }
  }
  Pt2setedPts<<input1[idx_inVec1][idx_inVec2],input3[0],min_dis;
  return Pt2setedPts;
}

Eigen::Vector3f calculate_groundClass::get2nearstPts(std::vector<Eigen::Vector3f> input){
  Eigen::Vector3f twonearestpcIdx;
  int numpc = input.size();
  float min_dis = 1000;
  if(numpc!=0){
    for(int i=0;i<numpc-1;i++){
      for(int j=i+1;j<numpc;j++){
        float dis_tmp = calculate_dis(input[i][1]-input[j][1],input[i][2]-input[j][2]);
        if(dis_tmp<min_dis){
          min_dis = dis_tmp;
          twonearestpcIdx<<input[i][0],input[j][0],min_dis;
        }
      }
    }
  }
  else{
    twonearestpcIdx<<0,0,0;
  }
  return twonearestpcIdx;
}

int calculate_groundClass::return_idx_in_matrix(std::vector<Eigen::Vector3f> input,int idx){
  int idx_in_matrix;
  for(int i=0;i<input.size();i++){
    if(input[i][0]==idx){
      idx_in_matrix = i;
    }
  }
  return idx_in_matrix;
}

std::vector<Eigen::Vector3f> calculate_groundClass::returnDisof2Pt(pcl::PointCloud<pcl::PointXYZ> input){//calculate dis of two points,return index of the 2 points and dis
  std::vector<Eigen::Vector3f> disOf2PointsinTurn;
  int numpc = input.size();

  if(numpc!=0){
    std::vector<Eigen::Vector3f> pcCoordinateWithIndex=transfer2Matrix(input);
    Eigen::Vector3f twonearestpcIdx = get2nearstPts(pcCoordinateWithIndex);
    Eigen::Vector3f Pt2setedPts;
    disOf2PointsinTurn.push_back(twonearestpcIdx);
    int idx1_mat = return_idx_in_matrix(pcCoordinateWithIndex,disOf2PointsinTurn[0][0]);
    pcCoordinateWithIndex.erase(pcCoordinateWithIndex.begin()+idx1_mat);
    int idx2_mat = return_idx_in_matrix(pcCoordinateWithIndex,disOf2PointsinTurn[0][1]);
    pcCoordinateWithIndex.erase(pcCoordinateWithIndex.begin()+idx2_mat);
    while(pcCoordinateWithIndex.size()!=0){
      float min_dis=1000;
      int idx=0;
      for(int i=0;i<pcCoordinateWithIndex.size();i++){
        Eigen::Vector3f Pt2setedPtstmp = newPt2setedPts(disOf2PointsinTurn,input,pcCoordinateWithIndex[i]);
        if(Pt2setedPtstmp[2]<min_dis){
          min_dis=Pt2setedPtstmp[2];
          Pt2setedPts = Pt2setedPtstmp;
          idx=i;
        }
      }
      disOf2PointsinTurn.push_back(Pt2setedPts);
      pcCoordinateWithIndex.erase(pcCoordinateWithIndex.begin()+idx);
    }

  }
  else{
    Eigen::Vector3f tmp;
    tmp<<0,0,0;
    disOf2PointsinTurn.push_back(tmp);
  }
  return disOf2PointsinTurn;
}

std::vector<Eigen::Vector3f> calculate_groundClass::sort_disinTurn(pcl::PointCloud<pcl::PointXYZ> input){
  std::vector<Eigen::Vector3f> disOf2PointsinTurn=returnDisof2Pt(input);
  std::vector<Eigen::Vector3f> sortedDisMatrix=disOf2PointsinTurn;

  int num_sorted=sortedDisMatrix.size();

  for(int i=0;i<num_sorted;i++){
    float min_dis=1000;
    int num_unsort=disOf2PointsinTurn.size();
    int index_to_delete;
    for(int j=0;j<num_unsort;j++){
      if(disOf2PointsinTurn[j][2]<min_dis){
        index_to_delete = j;
        min_dis = disOf2PointsinTurn[j][2];
      }
    }
    sortedDisMatrix[i] = disOf2PointsinTurn[index_to_delete];

    disOf2PointsinTurn.erase(disOf2PointsinTurn.begin()+index_to_delete);
  }
  return sortedDisMatrix;
}

int calculate_groundClass::find_same_indexwithInt(Eigen::Vector3f input1,int input2){
  int index=-1;

  Eigen::Vector2f tmp;
  tmp<<input1[0],input1[1];
  for(int i=0;i<2;i++){
    if(tmp[i]==input2){
      index = tmp[i];
    }
  }
  return index;
}

int calculate_groundClass::find_same_index(Eigen::Vector3f input1,Eigen::Vector3f input2){
  int index=-1;
  Eigen::Vector4f tmp;
  tmp<<input1[0],input1[1],input2[0],input2[1];
  for(int i=0;i<3;i++){
    for(int j=1;j<4;j++){
      if(tmp[i]==tmp[j]){
        index = tmp[i];
      }
    }
  }
  return index;
}

int calculate_groundClass::find_different_indexwithInt(Eigen::Vector3f input1,int input2){
  int index=-1;

  Eigen::Vector2f tmp;
  tmp<<input1[0],input1[1];
  for(int i=0;i<2;i++){
    if(tmp[i]!=input2){
      index = tmp[i];
    }
  }
  return index;
}

Eigen::Vector2f calculate_groundClass::restoredPt(Eigen::Vector4f parameters,bool leftofline,float X_in,float Y_in,float delta_Y, float delta_X,float dis,bool up){
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];
  Eigen::Vector2f pt;
  float A = (pow(bl*Y_in/al,2)+1);
  float B = 2*bl/al*(y0-bl*x0/al-Y_in)-2*X_in;
  float C = 2*X_in + Y_in*Y_in +bl*x0*Y_in/al -2*Y_in*y0 ;
  float X_out,Y_out;
  cout<<"al:"<<al<<",bl:"<<bl<<endl;
  float k=bl/al;
  float b=y0-al*x0/bl;
  float X_1 = (-B + sqrt(B*B-4*A*C))/(2*A);
  float X_2 = (-B - sqrt(B*B-4*A*C))/(2*A);
  float Y_1 = bl*(X_1-x0)/al + y0;
  float Y_2 = bl*(X_2-x0)/al + y0;
  cout<<"X_1:"<<X_1<<",X_2:"<<X_2<<endl;
  cout<<"Y_1:"<<Y_1<<",Y_2:"<<Y_2<<endl;
  cout<<"X_in:"<<X_in<<",Y_in:"<<Y_in<<endl;
  cout<<"delta_Y:"<<delta_Y<<",delta_X:"<<delta_X<<endl;
  cout<<"K:"<<k<<",b:"<<b<<endl;
  cout<<"dis:"<<dis<<endl;
  cout<<"up:"<<up<<endl;
  cout<<"left of line:"<<leftofline<<endl;
  if(leftofline){
    if(up){
      if(k*b>0){
        if(delta_X*delta_Y>0){
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
      else{
        if(delta_X*delta_Y>0){
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
    }
    else{
      if(k*b>0){
        if(delta_X*delta_Y>0){
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
      else{
        if(delta_X*delta_Y>0){
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
    }
  }
  else{
    if(up){
      if(k*b>0){
        if(delta_X*delta_Y>0){
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
      else{
        if(delta_X*delta_Y>0){
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
    }
    else{
      if(k*b>0){
        if(delta_X*delta_Y>0){
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
      else{
        if(delta_X*delta_Y>0){
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
        else{
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
        }
      }
    }
  }
  pt<<X_out,Y_out;
  return pt;

}



std::vector<Eigen::Vector4f> calculate_groundClass::indexthePC(pcl::PointCloud<pcl::PointXYZ> input,Eigen::Vector4f parameter,bool leftofline){
  int numpc=input.size();
  std::vector<Eigen::Vector3f> pcCoordinateWithIndex=transfer2Matrix(input);
  std::vector<Eigen::Vector3f> disOf2PointsinTurn=returnDisof2Pt(input);
  std::vector<Eigen::Vector4f> resortedpc_info;//
  Eigen::Vector2f HelpPoint = getHelpPoint(parameter);
  while(HelpPoint[0] ==0 || HelpPoint[1] == 0)
  {
    HelpPoint = getHelpPoint(parameter);
  }
  Eigen::Vector4f tmpvec;
  float a=0;
  int PtsCase;
  Eigen::Vector2f restoredXY;
  Eigen::Vector4f tmp,tmp1,tmp2,tmp3;
  float delta_X;
  float delta_Y;
  cout<<"numpc:"<<numpc<<endl;
  for(int i=0;i<numpc;i++){
    cout<<"index:"<<i<<",x:"<<input.points[i].x<<",y:"<<input.points[i].y<<endl;
  }
  if(numpc!=0){ // vermeide speicher fehler
    float dis1=disOf2PointsinTurn[0][2]; //shortest distance of two points
    float dis2=disOf2PointsinTurn[1][2];
    float dis3=disOf2PointsinTurn[2][2];
    int colMidindex = find_same_index(disOf2PointsinTurn[0],disOf2PointsinTurn[1]);  //the middle point in the 3 points
    int colNear1index,colNear2index;
    if(disOf2PointsinTurn[0][0]==colMidindex){colNear1index=disOf2PointsinTurn[0][1];}else{colNear1index=disOf2PointsinTurn[0][0];}
    if(disOf2PointsinTurn[1][0]==colMidindex){colNear2index=disOf2PointsinTurn[1][1];}else{colNear2index=disOf2PointsinTurn[1][0];}
    int HelpToNear1,HelpToMid,HelpToNear2;
    cout<<"dis1:"<<dis1<<"  dis2:"<<dis2<<"  dis3:"<<dis3<<endl;
    float y_sum_abs;
    float x_sum_abs;
    y_sum_abs = abs(input.points[colNear1index].y-input.points[colMidindex].y)+abs(input.points[colMidindex].y-input.points[colNear2index].y);
    x_sum_abs = abs(input.points[colNear1index].x-input.points[colMidindex].x)+abs(input.points[colMidindex].x-input.points[colNear2index].x);
    if(y_sum_abs>x_sum_abs){
      HelpToNear1 = (input.points[colNear1index].y-HelpPoint[1])/abs(input.points[colNear1index].y-HelpPoint[1]);
      cout<<"HelpToNear1 y:"<<HelpToNear1<<endl;
      HelpToMid = (input.points[colMidindex].y-HelpPoint[1])/abs(input.points[colMidindex].y-HelpPoint[1]);
      cout<<"HelpToNear2 y:"<<HelpToMid<<endl;
      HelpToNear2 = (input.points[colNear2index].y-HelpPoint[1])/abs(input.points[colNear2index].y-HelpPoint[1]);
      cout<<"HelpToNear3 y:"<<HelpToNear2<<endl;
    }
    else{
      HelpToNear1 = (input.points[colNear1index].x-HelpPoint[0])/abs(input.points[colNear1index].x-HelpPoint[0]);
      cout<<"HelpToNear1 y:"<<HelpToNear1<<endl;
      HelpToMid = (input.points[colMidindex].x-HelpPoint[0])/abs(input.points[colMidindex].x-HelpPoint[0]);
      cout<<"HelpToNear2 y:"<<HelpToMid<<endl;
      HelpToNear2 = (input.points[colNear2index].x-HelpPoint[0])/abs(input.points[colNear2index].x-HelpPoint[0]);
      cout<<"HelpToNear3 y:"<<HelpToNear2<<endl;
    }

    if(dis2/dis1>2.1&&dis2/dis1<3.3) //position: + +   +  -  -   - - or - -   -  -  +   + +
    {

      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase=1;//- -   -  -  +   + +
        cout<<"Case1:- -   -  -  +   + +"<<endl;
        a = dis1+dis2;
        tmpvec<<PtsCase,a,0,0;
        tmp<<6,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        delta_X = tmp1[2]-tmp2[2];
        delta_Y = tmp1[3]-tmp2[3];
        restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,2*a,false);
        tmp3<<1,0,restoredXY[0],restoredXY[1];
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info.push_back(tmp3);
      }
      else
      {
        PtsCase=2;//+ +   +  -  -   - -
        cout<<"Case2:+ +   +  -  -   - -"<<endl;
        a = dis1+dis2;
        tmpvec<<PtsCase,a,0,0;
        tmp<<2,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        delta_X = tmp2[2]-tmp1[2];
        delta_Y = tmp2[3]-tmp1[3];
        restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,2*a,true);
        tmp3<<7,0,restoredXY[0],restoredXY[1];
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info.push_back(tmp3);
      }
    }

    if(dis2/dis1>4.1&&dis2/dis1<5.5) ////position: + +   -  +  -   - - or - -   -  +  -   + -
    {
      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase=3;//- -   -  +  -   + +
        cout<<"Case3:- -   -  +  -   + +"<<endl;
        a = (dis1+dis2)*2/3;
        tmpvec<<PtsCase,a,0,0;
        tmp<<6,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        delta_X = tmp1[2]-tmp2[2];
        delta_Y = tmp1[3]-tmp2[3];
        restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,2*a,false);
        tmp3<<1,0,restoredXY[0],restoredXY[1];
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info.push_back(tmp3);
      }
      else
      {
        PtsCase=4;//+ +   -  +  -   - -
        cout<<"Case4:+ +   -  +  -   - -"<<endl;
        a = (dis1+dis2)*2/3;
        tmpvec<<PtsCase,a,0,0;
        tmp<<2,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        delta_X = tmp2[2]-tmp1[2];
        delta_Y = tmp2[3]-tmp1[3];
        restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,2*a,true);
        tmp3<<7,0,restoredXY[0],restoredXY[1];
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info.push_back(tmp3);
      }
    }

    if(dis2/dis1>0.9&&dis2/dis1<1.1)
    {
      if(dis3/dis2>0.9&& dis3/dis2<1.1){//position: + -   +  -  +   - +
        cout<<"Case5:+ -   +  -  +   - +"<<endl;
        a=dis1+dis2+dis3;
        PtsCase=5;
        tmpvec<<PtsCase,a,0,0;
        int colMid2index;
        for(int i=0;i<2;i++){
          for(int j=i+1;j<3;j++){
            if(find_same_index(disOf2PointsinTurn[i],disOf2PointsinTurn[j])!=-1&&find_same_index(disOf2PointsinTurn[i],disOf2PointsinTurn[j])!=colMidindex){
              colMid2index == find_same_index(disOf2PointsinTurn[i],disOf2PointsinTurn[j]);
            }
          }
        }
        if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid){
          tmp<<3,colMid2index,input.points[colMid2index].x,input.points[colMid2index].y;
          tmp1<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(find_same_indexwithInt(disOf2PointsinTurn[0],colMidindex)!=-1&&find_same_indexwithInt(disOf2PointsinTurn[0],colMid2index)!=-1){
            if(find_different_indexwithInt(disOf2PointsinTurn[1],colMidindex)!=-1){
              int tpidx = find_different_indexwithInt(disOf2PointsinTurn[1],colMidindex);
              tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
            }
            else{
              int tpidx = find_different_indexwithInt(disOf2PointsinTurn[1],colMid2index);
              tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
            }
          }
          else{
            if(find_different_indexwithInt(disOf2PointsinTurn[0],colMidindex)!=-1){
              int tpidx = find_different_indexwithInt(disOf2PointsinTurn[0],colMidindex);
              tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
            }
            else{
              int tpidx = find_different_indexwithInt(disOf2PointsinTurn[0],colMid2index);
              tmp2<<7,tpidx,input.points[tpidx].x,input.points[tpidx].y;
            }
          }
          delta_X = tmp2[2]-tmp1[2];
          delta_Y = tmp2[3]-tmp1[3];

          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
          tmp3<<1,0,restoredXY[0],restoredXY[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
        }
        else{

          float d1 = calculate_dis(input.points[colNear1index].x,input.points[colNear1index].y);
          float d3 = calculate_dis(input.points[colNear2index].x,input.points[colNear2index].y);
          if(d1<d3){
            tmp<<1,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
            tmp1<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
            tmp2<<5,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
          }
          else{
            tmp<<1,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
            tmp1<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
            tmp2<<5,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
          }
          delta_X = tmp2[2]-tmp1[2];
          delta_Y = tmp2[3]-tmp1[3];
          restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,true);
          tmp3<<7,0,restoredXY[0],restoredXY[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
        }

      }

      else{
        cout<<"Case6:- -   +  +  +   - -"<<endl;
        a=dis1+dis2;
        PtsCase=6;
        tmpvec<<PtsCase,a,0,0;
        float d1 = calculate_dis(input.points[colNear1index].x,input.points[colNear1index].y);
        float d3 = calculate_dis(input.points[colNear2index].x,input.points[colNear2index].y);
        if(d1<d3){
          tmp<<3,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
          tmp1<<4,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          tmp2<<5,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
        }
        else{
          tmp<<3,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
          tmp1<<4,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          tmp2<<5,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
        }
        delta_X = tmp1[2]-tmp[2];
        delta_Y = tmp1[3]-tmp[3];
        restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
        tmp3<<1,0,restoredXY[0],restoredXY[1];
        Eigen::Vector2f restoredXY1;
        restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,true);
        Eigen::Vector4f tmp4;
        tmp4<<7,0,restoredXY1[0],restoredXY1[1];
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info.push_back(tmp3);
        resortedpc_info.push_back(tmp4);
      }

    }

    if(dis2/dis1>1.35&&dis2/dis1<1.6)
    {
      if(dis3/dis2>0.65&& dis3/dis2<0.85){//position: - +   +  -  +   + -
        PtsCase=7;
        a=dis2;
        cout<<"Case7:- +   +  -  +   + -"<<endl;
        tmpvec<<PtsCase,a,0,0;
        if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
        {
          tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp1[2]-tmp2[2];
          delta_Y = tmp1[3]-tmp2[3];
          restoredXY = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,false);
          tmp3<<1,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
          Eigen::Vector4f tmp4;
          tmp4<<7,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
        else
        {

          tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp2[2]-tmp1[2];
          delta_Y = tmp2[3]-tmp1[3];
          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
          tmp3<<1,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,true);
          Eigen::Vector4f tmp4;
          tmp4<<7,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
      }


      if(dis3/dis2>0.85&& dis3/dis2<1.15){ //- +   +  -  +   - +
        if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
        {
          PtsCase = 8;
          a = dis2;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case8:+ -   +  -  +   + -"<<endl;
          tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }

          delta_X = tmp1[2]-tmp2[2];
          delta_Y = tmp1[3]-tmp2[3];
          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
          tmp3<<7,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,false);
          Eigen::Vector4f tmp4;
          tmp4<<1,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
        else
        {
          PtsCase = 9;
          a = dis2;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case9:- +   +  -  +   - +"<<endl;
          tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp2[2]-tmp1[2];
          delta_Y = tmp2[3]-tmp1[3];
          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
          tmp3<<1,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp2[2],tmp2[3],delta_Y,delta_X,a,true);
          Eigen::Vector4f tmp4;
          tmp4<<7,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
      }



      if(dis3/dis2>1.5&& dis3/dis2<1.8){//- +   +  +  -   + -
        if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
        {
          PtsCase = 10;
          a = (dis1+dis2)*0.8;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case10:- +   -  +  +   + -"<<endl;
          tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp2[2]-tmp1[2];
          delta_Y = tmp2[3]-tmp1[3];
          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
          tmp3<<7,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,false);
          Eigen::Vector4f tmp4;
          tmp4<<1,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
        else
        {
          PtsCase = 11;
          a = dis2;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case11:- +   +  +  -   + -"<<endl;
          tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp1[2]-tmp2[2];
          delta_Y = tmp1[3]-tmp2[3];
          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
          tmp3<<1,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,true);
          Eigen::Vector4f tmp4;
          tmp4<<7,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
      }

      if(dis3/dis2>1.8&& dis3/dis2<2.2){//- +   +  +  -   - + or + -   -  +  +   + -
        if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
        {
          PtsCase = 12;
          a = dis1*2;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case12:+ -   -  +  +   + -"<<endl;
          tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp2[2]-tmp1[2];
          delta_Y = tmp2[3]-tmp1[3];
          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,true);
          tmp3<<7,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,a*1.5,false);
          Eigen::Vector4f tmp4;
          tmp4<<1,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
        else
        {
          PtsCase = 13;
          a = dis2;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case13:- +   +  +  -   + -"<<endl;
          tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp1[2]-tmp2[2];
          delta_Y = tmp1[3]-tmp2[3];
          restoredXY = restoredPt(parameter,leftofline,tmp[2],tmp[3],delta_Y,delta_X,a,false);
          tmp3<<1,0,restoredXY[0],restoredXY[1];
          Eigen::Vector2f restoredXY1;
          restoredXY1 = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,true);
          Eigen::Vector4f tmp4;
          tmp4<<7,0,restoredXY1[0],restoredXY1[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
          resortedpc_info.push_back(tmp4);
        }
      }
    }

    if(dis2/dis1>1.9&&dis2/dis1<2.1)
    {
      if(dis3/dis2>1.15 && dis3/dis2<1.35){ //+ -   +  +  -   + - or - +   -  +  +   - +
        if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
        {
          PtsCase = 14;
          a = dis1*2;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case14:- +   -  +  +   - +"<<endl;
          tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<7,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<7,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<7,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<7,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          delta_X = tmp2[2]-tmp1[2];
          delta_Y = tmp2[3]-tmp1[3];
          restoredXY = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,false);
          tmp3<<1,0,restoredXY[0],restoredXY[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
        }
        else
        {
          PtsCase = 15;
          a = dis2;
          tmpvec<<PtsCase,a,0,0;
          cout<<"Case15:+ -   +  +  -   + -"<<endl;
          tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
          if(colMidindex!=disOf2PointsinTurn[0][0]){
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<1,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
              tmp2<<1,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }
          else{
            if(colMidindex!=disOf2PointsinTurn[1][0]){
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<1,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
            }
            else{
              tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
              tmp2<<1,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
            }
          }

          delta_X = tmp1[2]-tmp2[2];
          delta_Y = tmp1[3]-tmp2[3];
          restoredXY = restoredPt(parameter,leftofline,tmp1[2],tmp1[3],delta_Y,delta_X,1.5*a,true);
          tmp3<<7,0,restoredXY[0],restoredXY[1];
          resortedpc_info.push_back(tmpvec);
          resortedpc_info.push_back(tmp);
          resortedpc_info.push_back(tmp1);
          resortedpc_info.push_back(tmp2);
          resortedpc_info.push_back(tmp3);
        }
      }



    }
/*
    else{
      cout<<"points not in threshold"<<endl;
      Eigen::Vector4f tmpvec;
      tmpvec<<0,0,0,0;
      resortedpc_info.push_back(tmpvec);
    }
    */
  }

  else{
  cout<<"cant find valid points"<<endl;
  Eigen::Vector4f tmpvec;
  tmpvec<<0,0,0,0;
  resortedpc_info.push_back(tmpvec);
  }
  ROS_INFO("before sort:");
  for(int i=0;i<resortedpc_info.size();i++){
    cout<<resortedpc_info[i][0]<<","<<resortedpc_info[i][1]<<","<<resortedpc_info[i][2]<<","<<resortedpc_info[i][3]<<endl;
  }
  resortedpc_info = sortColIdx(resortedpc_info);
  ROS_INFO("After sort:");
  for(int i=0;i<resortedpc_info.size();i++){
    cout<<resortedpc_info[i][0]<<","<<resortedpc_info[i][1]<<","<<resortedpc_info[i][2]<<","<<resortedpc_info[i][3]<<endl;
  }
  return resortedpc_info;
}

Eigen::Vector2f calculate_groundClass::returnThetaSide(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameters){

  Eigen::Vector2f output;

  float theta=0;
  float side=0;//1:l 0:r
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];

  //line function: a(y-y0)-b(x-x0)=0
  float k=bl/al;

  float Phi=atan(k);

  float b=y0-bl*x0/al;
  float x1=resortedpc_info[1][2];
  float y1=resortedpc_info[1][3];
  float x2=resortedpc_info[2][2];
  float y2=resortedpc_info[2][3];
  float x3=resortedpc_info[3][2];
  float y3=resortedpc_info[3][3];

  bool y_judge=(((y3-y2)/abs(y3-y2))==((y2-y1)/abs(y2-y1)));
  bool x_judge=(((x3-x2)/abs(x3-x2))==((x2-x1)/abs(x2-x1)));


  float Xup = resortedpc_info[2][2];
  float Yup = resortedpc_info[2][3];
  //cout<<"in the theta xy7:"<<resortedpc_info[2][4]<<endl;

  float Xdown = resortedpc_info[1][2];

  float Ydown = resortedpc_info[1][3];


  if(b*(Xup-Xdown)<0){

    side=1;
    if(k*(Yup-Ydown)<0){
      theta = -Phi;
    }
    if(k*(Yup-Ydown)>0){
      theta = Phi+1/2*Phi*M_PI/abs(Phi);
    }

  }

  if(b*(Xup-Xdown)>0){
    side=0;
    if(k*(Yup-Ydown)<0){
      theta = -Phi;
    }
    if(k*(Yup-Ydown)>0){
      theta = Phi+Phi*1/2*M_PI/abs(Phi);
    }

  }

  output<<theta,side;
  return output;
}


Eigen::Vector2f calculate_groundClass::returnInitialXY(Eigen::Vector4f conerXY,Eigen::Vector4f parameters){
  float x = get_dis2line(parameters);
  float dis = calculate_dis(conerXY[2],conerXY[3]);
  float y = sqrt(pow(dis,2)-pow(x,2));
  Eigen::Vector2f InitialXY;
  InitialXY<<x,y;
  return InitialXY;
}


Eigen::Vector2f calculate_groundClass::getHelpPoint(Eigen::Vector4f parameters){
  Eigen::Vector2f Output;
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];
  //line function: a(y-y0)-b(x-x0)=0
  float k=bl/al;
  float Phi=atan(k);
  float b=y0-bl*x0/al;
  float x_help = -k*b/(pow(k,2)+1);
  float y_help = b/(pow(k,2)+1);
  if(x_help!=x_help || y_help!=y_help)
  {
    Output<<0,0;
  }
  else
  {
    Output<<x_help,y_help;
  }
  return Output;
}

int calculate_groundClass::findminColIdxinVec(std::vector<Eigen::Vector4f> resortedpc_info){
  int numpc = resortedpc_info.size();
  int minIdx=100;
  int IdxInVec;
  for(int i=0;i<numpc;i++){
    if(resortedpc_info[i][0]<minIdx){
      minIdx = resortedpc_info[i][0];
      IdxInVec = i;
    }
  }
  return IdxInVec;
}

std::vector<Eigen::Vector4f> calculate_groundClass::sortColIdx(std::vector<Eigen::Vector4f> resortedpc_info){

  int numpc = resortedpc_info.size();
  std::vector<Eigen::Vector4f> sortColIdx_info;
  sortColIdx_info.push_back(resortedpc_info[0]);
  std::vector<Eigen::Vector4f> col_info(resortedpc_info.begin()+1,resortedpc_info.end());
  int j=0;
  for(int i=0;i<numpc-1;i++){
    int minIdxInVec = findminColIdxinVec(col_info);
    sortColIdx_info.push_back(col_info[minIdxInVec]);
    col_info.erase(col_info.begin()+minIdxInVec);
    j++;
  }
  return sortColIdx_info;
}
