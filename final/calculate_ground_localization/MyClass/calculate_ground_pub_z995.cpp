#include "calculate_ground_localization/localization_zhang_pub.h"

bool calculate_groundClass::calculate_Position_callback(calculate_ground_localization::ground_info::Request &req,calculate_ground_localization::ground_info::Response &res){
  if(req.start==true)
  {
    gd_info_srv.request.start.data=true;
    pts_infoclient.call(gd_info_srv);
    check = gd_info_srv.response.checktwolines.data;
    ROS_INFO_STREAM("check signal:" << check);
    if(check){
      if(gd_info_srv.response.line1_parameters.data[0]*gd_info_srv.response.line1_parameters.data[1]*gd_info_srv.response.line1_parameters.data[2]*gd_info_srv.response.line1_parameters.data[3]!=0 &&
      gd_info_srv.response.line2_parameters.data[0]*gd_info_srv.response.line2_parameters.data[1]*gd_info_srv.response.line2_parameters.data[2]*gd_info_srv.response.line2_parameters.data[3]!=0)
      {
        parameter_1<<gd_info_srv.response.line1_parameters.data[0],gd_info_srv.response.line1_parameters.data[1],gd_info_srv.response.line1_parameters.data[2],gd_info_srv.response.line1_parameters.data[3];
        parameter_2<<gd_info_srv.response.line2_parameters.data[0],gd_info_srv.response.line2_parameters.data[1],gd_info_srv.response.line2_parameters.data[2],gd_info_srv.response.line2_parameters.data[3];
        Final_b = calculate_b(parameter_1,parameter_2);
        input_line1 = gd_info_srv.response.line1;
        input_line2 = gd_info_srv.response.line2;
        pcl::fromROSMsg(input_line1,pc_line1_filtered);
        pcl::fromROSMsg(input_line2,pc_line2_filtered);

        original_pc_pub_1.publish(input_line1);
        std::vector<Eigen::Vector4f> resortedpc_info1;
        std::vector<Eigen::Vector4f> restoredpc_info1;
        resortedpc_info1 = indexthePC(pc_line1_filtered,parameter_1);
        restoredpc_info1 = restoreLine(resortedpc_info1,parameter_1);
        pcl::PointCloud<pcl::PointXYZ> pctest1;
        pctest1=makepc(restoredpc_info1,pc_line1_filtered);
        sensor_msgs::PointCloud2 pt2test1;
        pcl::toROSMsg(pctest1,pt2test1);
        reseted_pt_pub_1.publish(pt2test1);

        original_pc_pub_2.publish(input_line2);
        std::vector<Eigen::Vector4f> resortedpc_info2;
        std::vector<Eigen::Vector4f> restoredpc_info2;
        resortedpc_info2 = indexthePC(pc_line2_filtered,parameter_2);
        restoredpc_info2 = restoreLine(resortedpc_info2,parameter_2);
        pcl::PointCloud<pcl::PointXYZ> pctest2;
        pctest2=makepc(restoredpc_info2,pc_line2_filtered);
        sensor_msgs::PointCloud2 pt2test2;
        pcl::toROSMsg(pctest2,pt2test2);
        reseted_pt_pub_2.publish(pt2test2);

        Eigen::Vector2f theta_side = get_theta(resortedpc_info1,parameter_1);
        string side;
        if(theta_side[1]){
          side = "left";
          line1_leftside = true;
        }
        else{
          side = "right";
          line1_leftside = false;
        }

/***********************************************************



********************************************************************/
        a_line1 = restoredpc_info1[0][1];
        a_line2 = restoredpc_info2[0][1];
        Line1PtsCase = restoredpc_info1[0][0];
        Line2PtsCase = restoredpc_info2[0][0];
        ROS_INFO_STREAM("a_line1:" << a_line1 <<"a_line2:" << a_line2);
        if(abs(a_line1-a_line2) <0.1){
          Final_a = (a_line1+a_line2)/2;
          ROS_INFO("a1-a2<10");
        }
        else if(Line1PtsCase==5 || Line2PtsCase==2){
          Final_a = a_line2;
          ROS_INFO("Line1PtsCase==5 || Line2PtsCase==2");
        }
        else if(Line2PtsCase==2 || Line1PtsCase==5){
          Final_a = a_line1;
          ROS_INFO("Line2PtsCase==2 || Line1PtsCase==5");
        }
        else if(a_line1>1 && a_line2>1){
          Final_a = (a_line1+a_line2)/2;
          ROS_INFO("a_line1>1 && a_line2>1");
        }
        else{
          ROS_INFO("return max!");

          if(a_line1>a_line2){
            Final_a = a_line1;
          }
          else{
            Final_a = a_line2;
          }
        }

        Eigen::Vector4f ConerXY = restoredpc_info1[1];
        Eigen::Vector2f InitialXY;
        InitialXY = returnInitialXY(ConerXY,parameter_1);
        ROS_INFO("received request");
        res.a = Final_a;
        res.b = Final_b;
        res.theta=theta_side[0];
        res.x_ = InitialXY[0];
        res.y_ = InitialXY[1];
        res.place = side;
      }
      else{
        res.a = 0;
        res.b = 0;
        res.theta=0;
        res.x_ = 0;
        res.y_ = 0;
        res.place = "unknown";
      }
    }
    else{
      res.a = 0;
      res.b = 0;
      res.theta=0;
      res.x_ = 0;
      res.y_ = 0;
      res.place = "unknown";
    }
  }

  return true;
}

float calculate_groundClass::calculate_dis(float x_, float y_){
  float distans = sqrt(pow(x_,2)+pow(y_,2));
  return distans;
}

float calculate_groundClass::get_dis2line(Eigen::Vector4f input){
  float x0=input[0];
  float y0=input[1];
  float a=input[2];
  float b=input[3];
  float dis = abs((a*y0-b*x0)/sqrt(a*a+b*b));
  return dis;
}

float calculate_groundClass::calculate_b(Eigen::Vector4f input1,Eigen::Vector4f input2){
  float b;
  b = get_dis2line(input1)+get_dis2line(input2);
  return b;
}

std::vector<Eigen::Vector3f> calculate_groundClass::transfer2Matrix(pcl::PointCloud<pcl::PointXYZ> input){ //schon vermeiden specher fehler
  int numpc=input.size();
  std::vector<Eigen::Vector3f> pcCoordinateWithIndex;
  if(input.size()!=0){ //  vermeiden speicher fehler
    for(int i=0;i<numpc;i++){
      Eigen::Vector3f tmp;
      tmp<<i,input.points[i].x,input.points[i].y;
      pcCoordinateWithIndex.push_back(tmp);
    }
  }
  else{
    Eigen::Vector3f tmp;
    tmp<<0,0,0;
    pcCoordinateWithIndex.push_back(tmp);
  }
  return pcCoordinateWithIndex;
}

Eigen::Vector3f calculate_groundClass::newPt2setedPts(std::vector<Eigen::Vector3f> input1,pcl::PointCloud<pcl::PointXYZ> input2,Eigen::Vector3f input3){
  float min_dis = 1000;
  float tmp_dis;
  int idx_inVec;
  Eigen::Vector3f Pt2setedPts;
  int idx_inVec1;
  int idx_inVec2;
  for(int i=0;i<input1.size();i++){
    for(int j=0;j<2;j++){
      tmp_dis = calculate_dis(input2.points[input1[i][j]].x-input3[1],input2.points[input1[i][j]].y-input3[2]);
      if(tmp_dis<min_dis){
        min_dis = tmp_dis;
        idx_inVec1 = i;
        idx_inVec2 = j;
      }
    }
  }
  Pt2setedPts<<input1[idx_inVec1][idx_inVec2],input3[0],min_dis;
  return Pt2setedPts;
}

Eigen::Vector3f calculate_groundClass::get2nearstPts(std::vector<Eigen::Vector3f> input){
  Eigen::Vector3f twonearestpcIdx;
  int numpc = input.size();
  float min_dis = 1000;
  if(numpc!=0){
    for(int i=0;i<numpc-1;i++){
      for(int j=i+1;j<numpc;j++){
        float dis_tmp = calculate_dis(input[i][1]-input[j][1],input[i][2]-input[j][2]);
        if(dis_tmp<min_dis){
          min_dis = dis_tmp;
          twonearestpcIdx<<input[i][0],input[j][0],min_dis;
        }
      }
    }
  }
  else{
    twonearestpcIdx<<0,0,0;
  }
  return twonearestpcIdx;
}

int calculate_groundClass::return_idx_in_matrix(std::vector<Eigen::Vector3f> input,int idx){
  int idx_in_matrix;
  for(int i=0;i<input.size();i++){
    if(input[i][0]==idx){
      idx_in_matrix = i;
    }
  }
  return idx_in_matrix;
}

std::vector<Eigen::Vector3f> calculate_groundClass::returnDisof2Pt(pcl::PointCloud<pcl::PointXYZ> input){//calculate dis of two points,return index of the 2 points and dis
  std::vector<Eigen::Vector3f> disOf2PointsinTurn;
  int numpc = input.size();

  if(numpc!=0){
    std::vector<Eigen::Vector3f> pcCoordinateWithIndex=transfer2Matrix(input);
    Eigen::Vector3f twonearestpcIdx = get2nearstPts(pcCoordinateWithIndex);
    Eigen::Vector3f Pt2setedPts;
    disOf2PointsinTurn.push_back(twonearestpcIdx);
    int idx1_mat = return_idx_in_matrix(pcCoordinateWithIndex,disOf2PointsinTurn[0][0]);
    pcCoordinateWithIndex.erase(pcCoordinateWithIndex.begin()+idx1_mat);
    int idx2_mat = return_idx_in_matrix(pcCoordinateWithIndex,disOf2PointsinTurn[0][1]);
    pcCoordinateWithIndex.erase(pcCoordinateWithIndex.begin()+idx2_mat);
    while(pcCoordinateWithIndex.size()!=0){
      float min_dis=1000;
      int idx=0;
      for(int i=0;i<pcCoordinateWithIndex.size();i++){
        Eigen::Vector3f Pt2setedPtstmp = newPt2setedPts(disOf2PointsinTurn,input,pcCoordinateWithIndex[i]);
        if(Pt2setedPtstmp[2]<min_dis){
          min_dis=Pt2setedPtstmp[2];
          Pt2setedPts = Pt2setedPtstmp;
          idx=i;
        }
      }
      disOf2PointsinTurn.push_back(Pt2setedPts);
      pcCoordinateWithIndex.erase(pcCoordinateWithIndex.begin()+idx);
    }

  }
  else{
    Eigen::Vector3f tmp;
    tmp<<0,0,0;
    disOf2PointsinTurn.push_back(tmp);
  }
  return disOf2PointsinTurn;
}

std::vector<Eigen::Vector3f> calculate_groundClass::sort_disinTurn(pcl::PointCloud<pcl::PointXYZ> input){
  std::vector<Eigen::Vector3f> disOf2PointsinTurn=returnDisof2Pt(input);
  std::vector<Eigen::Vector3f> sortedDisMatrix=disOf2PointsinTurn;

  int num_sorted=sortedDisMatrix.size();

  for(int i=0;i<num_sorted;i++){
    float min_dis=1000;
    int num_unsort=disOf2PointsinTurn.size();
    int index_to_delete;
    for(int j=0;j<num_unsort;j++){
      if(disOf2PointsinTurn[j][2]<min_dis){
        index_to_delete = j;
        min_dis = disOf2PointsinTurn[j][2];
      }
    }
    sortedDisMatrix[i] = disOf2PointsinTurn[index_to_delete];

    disOf2PointsinTurn.erase(disOf2PointsinTurn.begin()+index_to_delete);
  }
  return sortedDisMatrix;
}

int calculate_groundClass::find_same_indexwithInt(Eigen::Vector3f input1,int input2){
  int index=-1;

  Eigen::Vector2f tmp;
  tmp<<input1[0],input1[1];
  for(int i=0;i<2;i++){
    if(tmp[i]==input2){
      index = tmp[i];
    }
  }
  return index;
}

int calculate_groundClass::find_same_index(Eigen::Vector3f input1,Eigen::Vector3f input2){
  int index=-1;
  Eigen::Vector4f tmp;
  tmp<<input1[0],input1[1],input2[0],input2[1];
  for(int i=0;i<3;i++){
    for(int j=1;j<4;j++){
      if(tmp[i]==tmp[j]){
        index = tmp[i];
      }
    }
  }
  return index;
}

int calculate_groundClass::find_different_indexwithInt(Eigen::Vector3f input1,int input2){
  int index=-1;

  Eigen::Vector2f tmp;
  tmp<<input1[0],input1[1];
  for(int i=0;i<2;i++){
    if(tmp[i]!=input2){
      index = tmp[i];
    }
  }
  return index;
}

pcl::PointCloud<pcl::PointXYZ> calculate_groundClass::makepc(std::vector<Eigen::Vector4f> newPc_coordinate,pcl::PointCloud<pcl::PointXYZ> original_pc){
  pcl::PointCloud<pcl::PointXYZ> outputPC;
  outputPC.header = original_pc.header;
  pcl::PointCloud<pcl::PointXYZ>::Ptr outputPCPtr(new pcl::PointCloud<pcl::PointXYZ>);
  outputPCPtr = outputPC.makeShared();
  for(int i = 0;i<newPc_coordinate.size();i++){
    pcl::PointXYZ point;
    point.x =newPc_coordinate[i][2];
    point.y =newPc_coordinate[i][3];
    outputPCPtr->push_back(point);
  }
  return *outputPCPtr;
}


/*
pcl::PointCloud<pcl::PointXYZ> calculate_groundClass::makepc_selb(std::vector<Eigen::Vector4f> newPc_coordinate){
  pcl::PointCloud<pcl::PointXYZ> outputPC;
  pcl::PointCloud<pcl::PointXYZ>::Ptr outputPCPtr(new pcl::PointCloud<pcl::PointXYZ>);
  outputPCPtr = outputPC.makeShared();
  for(int i = 0;i<newPc_coordinate.size();i++){
    pcl::PointXYZ point;
    point.x =newPc_coordinate[i][2];
    point.y =newPc_coordinate[i][3];
    outputPCPtr->push_back(point);
  return *outputPCPtr;
}

*/

Eigen::Vector2f calculate_groundClass::restoredPt(Eigen::Vector4f parameters,bool leftofline,float X_in,float Y_in,float delta_Y, float delta_X,float dis,bool up){
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];
  Eigen::Vector2f pt;


  float k=bl/al;
  float b=y0-bl*x0/al;
  ROS_INFO_STREAM("k:" << k);
  ROS_INFO_STREAM("b:" << b);

  float X_out,Y_out;

  float A = pow(k,2) + 1;
  float B = -2*(pow(k,2)+1)*X_in;
  float C = pow(X_in,2)*(pow(k,2)+1) - pow(dis,2);

  float X_1 = (-B + sqrt(B*B-4*A*C))/(2*A);
  float X_2 = (-B - sqrt(B*B-4*A*C))/(2*A);
  float Y_1 = k*X_1 + b;
  float Y_2 = k*X_2 + b;
  if(leftofline){
    if(up)
    {
      if(k*b>0)
      {
        if(delta_X*delta_Y>0)
        {
          if(X_1-X_in<0)
          {
            X_out = X_1;
            Y_out = Y_1;
          }
          else
          {
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" left of line, k*b>0,deltax*deltay>0,up");
        }
        else
        {
          if(X_1-X_in>0)
          {
            X_out = X_1;
            Y_out = Y_1;
          }
          else
          {
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" left of line, k*b>0,deltax*deltay<0,up");

        }
      }
      else
      {
        if(delta_X*delta_Y>0)
        {
          if(X_1-X_in>0)
          {
            X_out = X_1;
            Y_out = Y_1;
          }
          else
          {
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" left of line, k*b<0,deltax*deltay>0,up");

        }
        else
        {
          if(X_1-X_in<0)
          {
            X_out = X_1;
            Y_out = Y_1;
          }
          else
          {
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" left of line, k*b<0,deltax*deltay<0,up");
        }
      }
    }
    else{
      if(k*b>0){
        if(delta_X*delta_Y>0){
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" left of line, k*b>0,deltax*deltay>0,down");

        }
        else{
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;

          }
          ROS_INFO_STREAM(" left of line, k*b>0,deltax*deltay<0,down");

        }
      }
      else{
        if(delta_X*delta_Y>0){
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" left of line, k*b<0,deltax*deltay>0,down");

        }
        else{
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" left of line, k*b<0,deltax*deltay<0,down");

        }
      }
    }
  }
  else{
    if(up){
      if(k*b>0){
        if(delta_X*delta_Y>0){
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" right of line, k*b>0,deltax*deltay>0,up");

        }
        else{
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" right of line, k*b>0,deltax*deltay<0,up");

        }
      }
      else{
        if(delta_X*delta_Y>0){
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" right of line, k*b<0,deltax*deltay>0,up");

        }
        else{
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" right of line, k*b<0,deltax*deltay<0,up");

        }
      }
    }
    else{
      if(k*b>0){
        if(delta_X*delta_Y>0){
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" right of line, k*b>0,deltax*deltay>0,down");

        }
        else{
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
            ROS_INFO_STREAM(" right of line, k*b>0,deltax*deltay<0,down");
          }
        }
      }
      else{
        if(delta_X*delta_Y>0){
          if(X_1-X_in>0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" right of line, k*b<0,deltax*deltay>0,down");
        }
        else{
          if(X_1-X_in<0){
            X_out = X_1;
            Y_out = Y_1;
          }
          else{
            X_out = X_2;
            Y_out = Y_2;
          }
          ROS_INFO_STREAM(" right of line, k*b<0,deltax*deltay<0,down");

        }
      }
    }
  }
  //ROS_INFO_STREAM("restored by method1: X_1: "  << X_1  <<  ",Y_1: "<<Y_1 << ",X_2: "<<X_2 << ",Y_2"<<Y_2);
  //ROS_INFO_STREAM("X_out: " << X_out << " Y_out: "<< Y_out);
  pt<<X_out,Y_out;
  return pt;
}

std::vector<Eigen::Vector4f> calculate_groundClass::indexthePC(pcl::PointCloud<pcl::PointXYZ> input,Eigen::Vector4f parameter){
  int numpc=input.size();
  float k_of_line = parameter[3]/parameter[2];
  float b_of_line = parameter[1]-parameter[3]*parameter[0]/parameter[2];
  int leftofline_self;

  for(int i=0;i<numpc;i++){
    Eigen::Vector2f tmp = getpointinline(parameter,input.points[i].x,input.points[i].y);
    input.points[i].x = tmp[0];

    input.points[i].y = tmp[1];
  }



  std::vector<Eigen::Vector3f> pcCoordinateWithIndex=transfer2Matrix(input);
  std::vector<Eigen::Vector3f> disOf2PointsinTurn=returnDisof2Pt(input);
  std::vector<Eigen::Vector4f> resortedpc_info;//
  Eigen::Vector2f HelpPoint = getHelpPoint(parameter);

  Eigen::Vector4f tmpvec;
  float a=0;
  int PtsCase;
  Eigen::Vector2f restoredXY;
  Eigen::Vector4f tmp,tmp1,tmp2,tmp3;
  float delta_X;
  float delta_Y;

  if(numpc!=0){ // vermeide speicher fehler
    float dis1,dis2,dis3;
    if(disOf2PointsinTurn.size()>2){
      dis1=disOf2PointsinTurn[0][2]; //shortest distance of two points
      dis2=disOf2PointsinTurn[1][2];
      dis3=disOf2PointsinTurn[2][2];
    }
    else{
      dis1=disOf2PointsinTurn[0][2]; //shortest distance of two points
      dis2=disOf2PointsinTurn[1][2];
      dis3=0;
    }

    int colMidindex = find_same_index(disOf2PointsinTurn[0],disOf2PointsinTurn[1]);  //the middle point in the 3 points
    int colNear1index,colNear2index;
    if(disOf2PointsinTurn[0][0]==colMidindex){colNear1index=disOf2PointsinTurn[0][1];}else{colNear1index=disOf2PointsinTurn[0][0];}
    if(disOf2PointsinTurn[1][0]==colMidindex){colNear2index=disOf2PointsinTurn[1][1];}else{colNear2index=disOf2PointsinTurn[1][0];}
    int HelpToNear1,HelpToMid,HelpToNear2;
    //cout<<"HelpPoint:"<<HelpPoint[0]<<","<<HelpPoint[1]<<endl;
    ROS_INFO_STREAM("dis 1:" << dis1 << ", dis 2:" << dis2 << ", dis 3:" << dis3);
    float y_sum_abs;
    float x_sum_abs;
    y_sum_abs = abs(input.points[colNear1index].y-input.points[colMidindex].y)+abs(input.points[colMidindex].y-input.points[colNear2index].y);
    x_sum_abs = abs(input.points[colNear1index].x-input.points[colMidindex].x)+abs(input.points[colMidindex].x-input.points[colNear2index].x);
    if(y_sum_abs>x_sum_abs){
      HelpToNear1 = (input.points[colNear1index].y-HelpPoint[1])/abs(input.points[colNear1index].y-HelpPoint[1]);
      //cout<<"HelpToNear1 y:"<<HelpToNear1<<endl;
      HelpToMid = (input.points[colMidindex].y-HelpPoint[1])/abs(input.points[colMidindex].y-HelpPoint[1]);
      //cout<<"HelpToNear2 y:"<<HelpToMid<<endl;
      HelpToNear2 = (input.points[colNear2index].y-HelpPoint[1])/abs(input.points[colNear2index].y-HelpPoint[1]);
      //cout<<"HelpToNear3 y:"<<HelpToNear2<<endl;
    }
    else{
      HelpToNear1 = (input.points[colNear1index].x-HelpPoint[0])/abs(input.points[colNear1index].x-HelpPoint[0]);
      //cout<<"HelpToNear1 y:"<<HelpToNear1<<endl;
      HelpToMid = (input.points[colMidindex].x-HelpPoint[0])/abs(input.points[colMidindex].x-HelpPoint[0]);
      //cout<<"HelpToNear2 y:"<<HelpToMid<<endl;
      HelpToNear2 = (input.points[colNear2index].x-HelpPoint[0])/abs(input.points[colNear2index].x-HelpPoint[0]);
      //cout<<"HelpToNear3 y:"<<HelpToNear2<<endl;
    }

    if(dis2/dis1>2.1&&dis2/dis1<3.4) //position: + +   +  -  -   - - or - -   -  -  +   + +
    {

      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase=1;//- -   -  -  +   + +
        ROS_INFO_STREAM("Case1:- -   -  -  +   + +");
        a = dis1+dis2;
        tmpvec<<PtsCase,a,0,0;
        tmp<<6,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }

      else
      {
        PtsCase=2;//+ +   +  -  -   - -
        a = dis1+dis2;
        ROS_INFO_STREAM("Case2:+ +   +  -  -   - -   a= " <<a);
        tmpvec<<PtsCase,a,0,0;
        tmp<<2,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }
    }

    else if(dis2/dis1>1.0&&dis2/dis1<1.25 && dis1<1)
    {

      a=dis1+dis2;
      ROS_INFO_STREAM("Case3:- -   +  +  +   - -  a=" << a );
      PtsCase=3;
      tmpvec<<PtsCase,a,0,0;
      float d1 = calculate_dis(input.points[colNear1index].x,input.points[colNear1index].y);
      float d3 = calculate_dis(input.points[colNear2index].x,input.points[colNear2index].y);
      if(d1<d3){
        tmp<<3,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
        tmp1<<4,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        tmp2<<5,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
      }
      else{
        tmp<<3,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
        tmp1<<4,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        tmp2<<5,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
      }
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
      resortedpc_info = sortColIdx(resortedpc_info);
      float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
      if(b_of_line*delta_x<0)
      {
        leftofline_self = 1;
      }
      else{
        leftofline_self = 0;
      }
      resortedpc_info[0][2] = leftofline_self;
    }

    else if(dis2/dis1>1.0&&dis2/dis1<1.20 && dis1>0.80)//case 6 and case 10
    {
      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase=10;//- -   -  -  +   + +
        a = dis1;
        ROS_INFO_STREAM("Case10:- -   +  -  +   - + a=" << a);
        tmpvec<<PtsCase,a,0,0;
        float d1 = calculate_dis(input.points[colNear1index].x,input.points[colNear1index].y);
        float d3 = calculate_dis(input.points[colNear2index].x,input.points[colNear2index].y);
        tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(d1<d3){
          tmp1<<3,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
          tmp2<<7,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
        }
        else{
          tmp1<<3,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
          tmp2<<7,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }
      else
      {
        a=dis2;
        ROS_INFO_STREAM("Case6:+ -   +  -  +   - -  a=" << a );
        PtsCase=6;
        tmpvec<<PtsCase,a,0,0;
        float d1 = calculate_dis(input.points[colNear1index].x,input.points[colNear1index].y);
        float d3 = calculate_dis(input.points[colNear2index].x,input.points[colNear2index].y);
        tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(d1<d3){
          tmp1<<1,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
          tmp2<<5,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
        }
        else{
          tmp1<<1,colNear2index,input.points[colNear2index].x,input.points[colNear2index].y;
          tmp2<<5,colNear1index,input.points[colNear1index].x,input.points[colNear1index].y;
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }

    }

    else if(dis2/dis1>1.3 && dis2/dis1<1.7 && dis1<1)
    {
      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase = 12;
        a = dis1*2;
        tmpvec<<PtsCase,a,0,0;
        ROS_INFO_STREAM("Case12:- -   -  +  +   + -   a=" <<a);
        tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<6,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<6,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }

        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;

      }
      else
      {
        PtsCase = 4;
        a = dis1*2;
        tmpvec<<PtsCase,a,0,0;
        ROS_INFO_STREAM("Case4:- +   +  +  -   + -   a=" <<a);
        tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<2,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<2,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }

        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;

      }

    }

    else if(dis2/dis1>1.2 && dis2/dis1<1.6 && dis1>1)
    {
      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase = 15;
        a = dis2;
        tmpvec<<PtsCase,a,0,0;
        ROS_INFO_STREAM("Case15:- -   +  -  +   + -   a=" <<a);
        tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<6,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<6,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }

        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }
      else
      {
        PtsCase = 9;
        a = dis2;
        tmpvec<<PtsCase,a,0,0;
        ROS_INFO_STREAM("Case9:- +   +  -  +   - -   a=" <<a);
        tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<2,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<2,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }

        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }


    }

    else if(dis2/dis1>1.9 && dis2/dis1<2.1)
    {
      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase = 11;
        a = dis2;
        ROS_INFO_STREAM("Case11:- -   -  +  +   - +   a=" << a);
        tmpvec<<PtsCase,a,0,0;
        tmp<<5,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<7,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<7,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<7,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<7,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }
      else
      {
        PtsCase = 5;
        a = dis2;
        tmpvec<<PtsCase,a,0,0;
        ROS_INFO_STREAM("Case5:+ -   +  +  -   - - a= " << a);
        tmp<<3,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<1,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<1,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<1,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<4,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<1,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }

    }

    else if(dis2/dis1>4.1&&dis2/dis1<5.5) ////position: + +   -  +  -   - - or - -   -  +  -   + -
    {
      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase=13;//+ +   -  +  -   - -
        a = (dis1+dis2)*2/3;
        ROS_INFO_STREAM("Case13:- -   -  +  -   + +   a="  << a);
        tmpvec<<PtsCase,a,0,0;
        tmp<<6,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }
      else
      {
        PtsCase=7;//+ +   -  +  -   - -
        a = (dis1+dis2)*2/3;
        ROS_INFO_STREAM("Case7:+ +   -  +  -   - -  a="  << a);
        tmpvec<<PtsCase,a,0,0;
        tmp<<2,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<4,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<4,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }


    }

    else if(dis2/dis1>6.5&&dis2/dis1<7.5) ////position: + +   -  +  -   - - or - -   -  +  -   + -
    {
      if(HelpToNear1==HelpToMid&&HelpToNear2==HelpToMid)
      {
        PtsCase=14;//+ +   -  +  -   - -
        a = (dis1+dis2)*0.5;
        ROS_INFO_STREAM("Case14:- -   +  -  -   + +  a=" << a);

        tmpvec<<PtsCase,a,0,0;
        tmp<<6,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<7,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<3,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }
      else
      {
        PtsCase=8;//+ +   -  +  -   - -
        a = (dis1+dis2)*0.5;
        ROS_INFO_STREAM("Case8:+ +   -  -  +   - -  a=" << a);

        tmpvec<<PtsCase,a,0,0;
        tmp<<2,colMidindex,input.points[colMidindex].x,input.points[colMidindex].y;
        if(colMidindex!=disOf2PointsinTurn[0][0]){
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][0],input.points[disOf2PointsinTurn[0][0]].x,input.points[disOf2PointsinTurn[0][0]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        else{
          if(colMidindex!=disOf2PointsinTurn[1][0]){
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][0],input.points[disOf2PointsinTurn[1][0]].x,input.points[disOf2PointsinTurn[1][0]].y;
          }
          else{
            tmp1<<1,disOf2PointsinTurn[0][1],input.points[disOf2PointsinTurn[0][1]].x,input.points[disOf2PointsinTurn[0][1]].y;
            tmp2<<5,disOf2PointsinTurn[1][1],input.points[disOf2PointsinTurn[1][1]].x,input.points[disOf2PointsinTurn[1][1]].y;
          }
        }
        resortedpc_info.push_back(tmpvec);
        resortedpc_info.push_back(tmp);
        resortedpc_info.push_back(tmp1);
        resortedpc_info.push_back(tmp2);
        resortedpc_info = sortColIdx(resortedpc_info);
        float delta_x = resortedpc_info[2][2] - resortedpc_info[1][2];
        if(b_of_line*delta_x<0)
        {
          leftofline_self = 1;
        }
        else{
          leftofline_self = 0;
        }
        resortedpc_info[0][2] = leftofline_self;
      }

    }

    else
    {
      ROS_INFO_STREAM("proportion not in thereshold!");
      Eigen::Vector4f tmpvec;
      tmpvec<<0,0,0,0;
      tmp<<0,0,0,0;
      tmp1=tmp;
      tmp2=tmp;
      resortedpc_info.push_back(tmpvec);
      resortedpc_info.push_back(tmp);
      resortedpc_info.push_back(tmp1);
      resortedpc_info.push_back(tmp2);
    }
  }

  else{
    ROS_INFO_STREAM("cant find valid points");
    Eigen::Vector4f tmpvec;
    tmpvec<<0,0,0,0;
    resortedpc_info.push_back(tmpvec);
  }
  return resortedpc_info;
}

std::vector<Eigen::Vector4f> calculate_groundClass::restoreLine(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameter){
  int PtsCase = resortedpc_info[0][0];
  float a = resortedpc_info[0][1];
  ROS_INFO_STREAM("!!!!!!a!!!!!!" << a);
  bool leftofline;
  Eigen::Vector2f restore1,restore2,restore3,restore4;
  Eigen::Vector4f restore1_1,restore2_1,restore3_1,restore4_1;
  if(resortedpc_info[0][2] == 1)
  {
    leftofline = true;
  }
  else{
    leftofline = false;
  }
  switch (PtsCase)
  {
    case 1:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.75*a,false);
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.5*a,false);
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,false);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.25*a,false);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<3,0,restore3[0],restore3[1];
      restore4_1<<4,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 2:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.25*a,true);
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,true);
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.5*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.75*a,true);
      restore1_1<<4,0,restore1[0],restore1[1];
      restore2_1<<5,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 3:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.5*a,false);
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.25*a,false);
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.25*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.5*a,true);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 4:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,true);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,true);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<5,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 5:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,true);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,true);
      restore1_1<<2,0,restore1[0],restore1[1];
      restore2_1<<5,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 6:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.5*a,true);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,true);
      restore1_1<<2,0,restore1[0],restore1[1];
      restore2_1<<4,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 7:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,true);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,true);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.5*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.75*a,true);
      restore1_1<<3,0,restore1[0],restore1[1];
      restore2_1<<5,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 8:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,true);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.25*a,true);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.5*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.75*a,true);
      restore1_1<<3,0,restore1[0],restore1[1];
      restore2_1<<4,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 9:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.5*a,true);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,true);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,true);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<4,0,restore2[0],restore2[1];
      restore3_1<<6,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 10:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,false);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.5*a,false);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,true);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<4,0,restore3[0],restore3[1];
      restore4_1<<6,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 11:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,false);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,false);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,true);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<3,0,restore3[0],restore3[1];
      restore4_1<<6,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 12:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,false);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,false);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,true);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<3,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 13:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.75*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.5*a,false);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,false);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,false);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<3,0,restore3[0],restore3[1];
      restore4_1<<5,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 14:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.75*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2.5*a,false);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.25*a,false);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.75*a,false);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<4,0,restore3[0],restore3[1];
      restore4_1<<5,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;
    case 15:
      restore1 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],2*a,false);//1
      restore2 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],1.75*a,false);//5
      restore3 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],0.5*a,false);
      restore4 = restoredPt(parameter,leftofline,resortedpc_info[2][2],resortedpc_info[2][3],resortedpc_info[2][3]-resortedpc_info[1][3],resortedpc_info[2][2]-resortedpc_info[1][2],a,true);
      restore1_1<<1,0,restore1[0],restore1[1];
      restore2_1<<2,0,restore2[0],restore2[1];
      restore3_1<<4,0,restore3[0],restore3[1];
      restore4_1<<7,0,restore4[0],restore4[1];
      resortedpc_info.push_back(restore1_1);
      resortedpc_info.push_back(restore2_1);
      resortedpc_info.push_back(restore3_1);
      resortedpc_info.push_back(restore4_1);
      resortedpc_info = sortColIdx(resortedpc_info);
      break;


  }
  return resortedpc_info;
}

Eigen::Vector2f calculate_groundClass::returnThetaSide(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameters,bool left_of_line){

  Eigen::Vector2f output;

  float theta=0;
  float side=0;//1:l 0:r
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];
  //line function: a(y-y0)-b(x-x0)=0
  float k=bl/al;
  float Phi=atan(k);
  float b=y0-bl*x0/al;

  if(left_of_line){
    side=1;
    if(k>0){
      if(b>0){
        theta=-Phi;
      }
      else{
        theta=Phi+1/2*M_PI;
      }
    }
    else{
      if(b>0){
        theta=-Phi;
      }
      else{
        theta=Phi-1/2*M_PI;
      }
    }
  }
  else{
    side=0;
    if(k>0){
      if(b>0){
        theta=Phi+1/2*M_PI;
      }
      else{
        theta=-Phi;
      }
    }
    else{
      if(b>0){
        theta=Phi-1/2*M_PI;
      }
      else{
        theta=-Phi;
      }
    }
  }

  output<<theta,side;
  return output;
}

Eigen::Vector2f calculate_groundClass::get_theta(std::vector<Eigen::Vector4f> resortedpc_info,Eigen::Vector4f parameters){
  float theta=0;
  float calculate_side;
  Eigen::Vector2f output;
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];
  float k=bl/al;
  float Phi=atan(k);
  float b=y0-bl*x0/al;
  bool y_judge = (((resortedpc_info[3][3]-resortedpc_info[2][3])/abs(resortedpc_info[3][3]-resortedpc_info[2][3]))==((resortedpc_info[2][3]-resortedpc_info[1][3])/abs(resortedpc_info[2][3]-resortedpc_info[1][3])));
  bool x_judge = (((resortedpc_info[3][2]-resortedpc_info[2][2])/abs(resortedpc_info[3][2]-resortedpc_info[2][2]))==((resortedpc_info[2][2]-resortedpc_info[1][2])/abs(resortedpc_info[2][2]-resortedpc_info[1][2])));
  ROS_INFO_STREAM("pt1:" << resortedpc_info[3][2] << "," << resortedpc_info[3][3] );
  ROS_INFO_STREAM("pt2:" << resortedpc_info[1][2] << "," << resortedpc_info[1][3] );

  float delta_y = resortedpc_info[3][3]-resortedpc_info[1][3];
  float delta_x = resortedpc_info[3][2]-resortedpc_info[1][2];
  cout<<"b*delta x:"<<b*delta_x<<endl;
  if(delta_x*b<0){
    calculate_side = 1;
  }
  else{
    calculate_side = 0;
  }

  cout<<"calculate left of line:"<<calculate_side<<endl;
  cout<<"k:"<<k<<","<<"b:"<<b<<endl;
  cout<<"delta_x:"<<delta_x<<"delta_y:"<<delta_y<<endl;
  cout<<"x_judge:"<<x_judge<<",y_judge:"<<y_judge<<endl;


  if(calculate_side){
    if(y_judge&&x_judge){
      if(k*b>0){
        if(delta_x*delta_y>0){
          cout<<"theta case1"<<endl;
          theta = -Phi;
        }
        else{
          cout<<"theta case2"<<endl;
          theta = -(Phi+3.1415926);
        }
      }
      else{
        if(delta_x*delta_y>0){
          cout<<"theta case3"<<endl;
          theta = 3.1415926-Phi;
        }
        else{
          cout<<"theta case4"<<endl;
          theta = -Phi;
        }
      }
    }

  }
  else{
    if(y_judge&&x_judge){
      if(k*b>0)
      {
        if(delta_x*delta_y>0)
        {
          cout<<"theta case5"<<endl;
          theta = 3.1415926-Phi;
        }
        else
        {
          cout<<"theta case6"<<endl;
          theta = -Phi;
        }
      }
      else
      {
        if(delta_x*delta_y>0)
        {
          cout<<"theta case7"<<endl;
          theta = -Phi;
        }
        else
        {
          cout<<"theta case8"<<endl;
          theta = -(3.1415926+Phi);
        }
      }
    }
  }
  output<<theta,calculate_side;
  return output;
}

Eigen::Vector2f calculate_groundClass::returnInitialXY(Eigen::Vector4f conerXY,Eigen::Vector4f parameters){
  float x = get_dis2line(parameters);

  float dis = calculate_dis(conerXY[2],conerXY[3]);
  float y = sqrt(pow(dis,2)-pow(x,2));
  Eigen::Vector2f InitialXY;
  InitialXY<<x,y;
  return InitialXY;
}

Eigen::Vector2f calculate_groundClass::getHelpPoint(Eigen::Vector4f parameters){
  Eigen::Vector2f Output;
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];
  //line function: a(y-y0)-b(x-x0)=0
  float k=bl/al;
  float Phi=atan(k);
  float b=y0-bl*x0/al;
  float x_help = -k*b/(pow(k,2)+1);
  float y_help = b/(pow(k,2)+1);
  if(x_help!=x_help || y_help!=y_help)
  {
    Output<<0,0;
  }
  else
  {
    Output<<x_help,y_help;
  }
  return Output;

}

Eigen::Vector2f calculate_groundClass::getpointinline(Eigen::Vector4f parameters,float x_0,float y_0){
  Eigen::Vector2f Output;
  float x0=parameters[0];
  float y0=parameters[1];
  float al=parameters[2];
  float bl=parameters[3];
  //line function: a(y-y0)-b(x-x0)=0
  float k=bl/al;
  float Phi=atan(k);
  float b=y0-bl*x0/al;
  float x_help = (x_0+k*y_0-k*b)/(pow(k,2)+1);
  float y_help = k*x_help+b;
  if(x_help!=x_help || y_help!=y_help)
  {
    Output<<0,0;
  }
  else
  {
    Output<<x_help,y_help;
  }
  return Output;

}

int calculate_groundClass::findminColIdxinVec(std::vector<Eigen::Vector4f> resortedpc_info){
  int numpc = resortedpc_info.size();
  int minIdx=100;
  int IdxInVec;
  for(int i=0;i<numpc;i++){
    if(resortedpc_info[i][0]<minIdx){
      minIdx = resortedpc_info[i][0];
      IdxInVec = i;
    }
  }
  return IdxInVec;
}

std::vector<Eigen::Vector4f> calculate_groundClass::sortColIdx(std::vector<Eigen::Vector4f> resortedpc_info){

  int numpc = resortedpc_info.size();
  std::vector<Eigen::Vector4f> sortColIdx_info;
  sortColIdx_info.push_back(resortedpc_info[0]);
  std::vector<Eigen::Vector4f> col_info(resortedpc_info.begin()+1,resortedpc_info.end());
  int j=0;
  for(int i=0;i<numpc-1;i++){
    int minIdxInVec = findminColIdxinVec(col_info);
    sortColIdx_info.push_back(col_info[minIdxInVec]);
    col_info.erase(col_info.begin()+minIdxInVec);
    j++;
  }
  return sortColIdx_info;
}
