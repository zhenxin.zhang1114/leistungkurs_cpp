#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/Point32.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <laser_geometry/laser_geometry.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <math.h>
#include <vector>
#include "localization/dbscan.h"
#include <sensor_msgs/point_cloud_conversion.h>

#include <pcl_ros/point_cloud.h> // enable pcl publishing
#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/common_headers.h>
ros::Publisher lidar_pub;
ros::Publisher lidar_pubdense;
sensor_msgs::LaserScan lidar_msg_stable;
int counter=0;
const int length=360;
const int MINIMUM_POINTS=6;
const int MAX_POINTS= 5;
const float EPSILON=0.05;
const double scanmaxrange=3.4;
int filtertime=0;
const double p_epsilon=0.1;
const int countertime=10;
const float dismin=0.01;

using namespace std;

//Minimumpunkte rechnen
void Scanmin(const sensor_msgs::LaserScanConstPtr& lidar_msg)
{
  for (int i = 0; i < length; i++)
  {
    lidar_msg_stable.ranges[i] = min(lidar_msg->ranges[i],lidar_msg_stable.ranges[i]);
  }
}

//PCL zum Vektor
vector<Point> PC2V (const sensor_msgs::PointCloud lidar_pcl){
  unsigned int  num_points=lidar_pcl.points.size();

  vector<Point> points;
  Point *p = (Point *)calloc(num_points, sizeof(Point));

  for (int i=0; i<num_points; i++){
        p[i].x=lidar_pcl.points[i].x;
        p[i].y=lidar_pcl.points[i].y;
        p[i].z=0;
        p[i].clusterID = UNCLASSIFIED;

        points.push_back(p[i]);
  }

  free(p);
  return points;
}

//Vektor zum PCL
sensor_msgs::PointCloud V2PC (vector<Point> points){
  unsigned int num_points=points.size();

  sensor_msgs::PointCloud pcl;

  geometry_msgs::Point32 point;

  for(int i=0;i<num_points;i++){
    point.x=points[i].x;
    point.y=points[i].y;
    point.z=0;
    pcl.points.push_back(point);
  }

  return pcl;
}

//Abstand zweier Punkten rechnen
inline double CalculateDistance( Point pointCore, Point pointTarget )
{
    return pow(pointCore.x - pointTarget.x,2)+pow(pointCore.y - pointTarget.y,2);
}

//Dichte Punkte sammeln
vector<Point> Takedensepoints(vector<Point> points)
{
  vector<Point> densep;
  for (auto i:points){
    int ti=0;
    for (auto j:points){
      if (CalculateDistance(i,j) < 0.002){
        ti++;
      }
    }
    if (ti > MAX_POINTS){
      densep.push_back(i);
    }
  }

  return densep;
}

void Update(vector<Point>& lidar_updatednow, vector<Point> points, Point p)
{
    int index = 0;
    vector<Point>::iterator iter;

    for( iter = points.begin(); iter != points.end(); ++iter)
    {
        double dis=CalculateDistance(p, *iter);
        if ( dis <= p_epsilon )
        {
            index++;
        }
    }
    if (index >0){
        lidar_updatednow.push_back(p);
    }
    index=0;

}

//LIDAR SIGNAL ausfiltern und ausgeben
void preprocessing(const sensor_msgs::LaserScanConstPtr& laser){
  int lasersize = laser->ranges.size();
  //ROS_INFO_STREAM ("laser size "<<lasersize);

  //for Bube
  //take minimum as stable points

  if(counter==0){
		lidar_msg_stable=*laser;
	}
  else{
    Scanmin(laser);
  }
  counter ++;

  if (counter==countertime){
      laser_geometry::LaserProjection projector_;
      sensor_msgs::PointCloud lidar_pcl;

      //filter points too far away
      projector_.projectLaser(lidar_msg_stable,lidar_pcl, scanmaxrange);

      vector<Point> lidar_points = PC2V(lidar_pcl);

      DBSCAN ds(MINIMUM_POINTS, EPSILON, lidar_points);

      ds.run();

      vector<Point> lidar_filtered;
      for (int i=0; i<ds.getTotalPointSize();i++){
        if (ds.m_points[i].clusterID == NOISE){
          lidar_filtered.push_back(ds.m_points[i]);
        }
      }

      vector<Point> lidar_densep = Takedensepoints(lidar_points);
      //ROS_INFO_STREAM("dense point "<< lidar_densep.size());

      lidar_filtered.insert(lidar_filtered.end(),lidar_densep.begin(),lidar_densep.end());

      sensor_msgs::PointCloud lidar_pcl_filtered;
      lidar_pcl_filtered=V2PC(lidar_filtered);
      //lidar_pcl_filtered=V2PC(lidar_merge);
      lidar_pcl_filtered.header=lidar_msg_stable.header;
      lidar_pub.publish(lidar_pcl_filtered);

      sensor_msgs::PointCloud lidar_pcl_dense;
      lidar_pcl_dense=V2PC(lidar_densep);
      lidar_pcl_dense.header=lidar_msg_stable.header;
      lidar_pubdense.publish(lidar_pcl_dense);

      counter=0;
      filtertime ++;
      //ROS_INFO("FILTER TIME IS %d",filtertime);

    }
}


int main(int argc, char **argv)
{
    ros::init(argc,argv,"localization_pcl");
    ros::NodeHandle n;
    lidar_pub = n.advertise<sensor_msgs::PointCloud>("/lidarscan_modified",10);
    lidar_pubdense = n.advertise<sensor_msgs::PointCloud>("/lidarscan_dense",10);
    //lidar_dispub = n.advertise<sensor_msgs::LaserScan>("/lidarscan_dismodified",10);
    unsigned char rate;
    rate = 10;
    ros::Rate loop_rate(rate);

  	ros::Subscriber laser_sub = n.subscribe("/lidarscan", 10, preprocessing);

    ros::spin();

    return 0;
}
